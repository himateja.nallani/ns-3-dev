#!/bin/bash

if [ "$indir" = "" ]; then
    indir="out"
fi

if [ "$outdir" = "" ]; then
    outdir="graphs"
fi

# TODO: 1) varying_bw: Also, plot delay vs time(SPROUT fig-1).
#	4) fct_short_flow: We should vary network load to obtain fcts of short flows(See PCC fig-15).
#	5) parkinglot: Simply tabulate the fairness results(See DCTCP fig-17).
#	6) Kill all the tcp ports that have been generated for each simulation run.
if [ "$tests" = "" ]; then
    tests="random_loss varying_buffer rtt_unfairness varying_bw convergence dynamics jain_fairness coexist fairness incast \
           shallow_buffer bufferbloat fct_short_flows satellite tradeoff wireless parkinglot"
fi

tests_done=""
tcp_variants="TcpCubic TcpBbr TcpNewReno TcpIllinois TcpVegas TcpDctcp"

mkdir $outdir

if [[ $tests == *"random_loss"* ]]; then
	echo "Running test: random_loss"
	if compgen -G "$outdir/random_loss*" > /dev/null; then
    		rm -r $outdir/random_loss*
	fi
    	loss_rates="0 0.0000000001 0.000000001 0.00000001 0.0000001 0.000001 0.00001 0.0001 0.001 0.01 0.02 0.03 0.04"
	tcp_senders=10
	for loss_rate in $loss_rates; do
	    printf "\n%s," $loss_rate >> $outdir/random_loss_table.csv
	    for tcp_variant in $tcp_variants; do
		set  +e
		./ns3 run --no-build "scratch/clusters --transportProtocol=$tcp_variant --runtime=100 \
	          --longFlows=$tcp_senders --shortFlows=0 --bottleneckLinkDataRate=100Mbps  --bottleneckLinkDelay=15ms \
	          --endNodeLinkCapacity=0Mbps --endNodeLinkDelay=0ms --queueDiscType=FifoQueueDisc --queueSize=50p \
		  --tracing=false --mtu=1400 --isSack=true --traceSamplingTime=10 --numberOfPackets=0 --isDataRateVariable=false \
		  --isRandomLossVariable=false --isDelayVariable=false --numberOfCoreRouters=2 --topology=singleLink \
		  --randomLossRate=$loss_rate --isRandomArrival=true"
		set -e
		throughput=( $(awk '/Throughput:/ {print($2)}' clustersFlowMonitor.dat) )
		if [[ $tcp_senders -gt 1 ]]
		then
			#this whole if block isn't working for now
			tx_bytes=( $(awk '/Tx Packets/ {print($5)}' clustersFlowMonitor.dat) )
			sum_tx_bytes=$( IFS="+"; bc <<< "${tx_bytes[*]}" )
			fct=( $(awk '/Flow completion time:/ {print($4)}' clustersFlowMonitor.dat) )
			sum_fct=$( IFS="+"; bc <<< "${fct[*]}" )
			avg_fct=$(echo "scale=2; $sum_fct / ${#fct[@]}" | bc -l)
			printf "%.2f," $(echo "$sum_tx_bytes / 8 / $avg_fct" | bc -l) >> $outdir/random_loss_table.csv
		else
			printf "%.4f," $throughput >> $outdir/random_loss_table.csv
		fi
	    done
	done
tests_done="$tests_done random_loss"
fi

if [[ $tests == *"varying_buffer"* ]]; then
	echo "Running test: varying_buffer"
	if compgen -G "$outdir/varying_buffer*" > /dev/null; then
    		rm -r $outdir/varying_buffer*
	fi
	#Here the BW-delay product is 100Mbps*15ms = 125pkts
    	buffer_sizes="1.5 3 9 15 30 45 60 90 105 120 150 180 240 300"
    	for buffer_size in $buffer_sizes; do
	    echo -e "$buffer_size," >> $outdir/varying_buffer_throughput.csv
	    printf "\n%s," $buffer_size >> $outdir/varying_buffer_drop_rate.csv
	    for tcp_variant in $tcp_variants; do
		set  +e
		./ns3 run --no-build "scratch/clusters --transportProtocol=$tcp_variant --runtime=100 \
	          --longFlows=1 --shortFlows=0 --bottleneckLinkDataRate=100Mbps  --bottleneckLinkDelay=15ms \
	          --endNodeLinkCapacity=0Mbps --endNodeLinkDelay=0us --queueDiscType=FifoQueueDisc --queueSize="${buffer_size}KB" \
		  --tracing=false --mtu=1400 --isSack=true --traceSamplingTime=10 --numberOfPackets=0 --isDataRateVariable=false \
         	  --isRandomLossVariable=false --isDelayVariable=false --numberOfCoreRouters=2 --topology=singleLink \
		  --randomLossRate=0 --isRandomArrival=true"
		set -e
		truncate -s-1 $outdir/varying_buffer_throughput.csv
		echo -e "$(awk '/Throughput:/ {print($2)}' clustersFlowMonitor.dat)," >> $outdir/varying_buffer_throughput.csv
		drops="$(awk '/Disc:/ {print($6)}' clustersFlowMonitor.dat)"
		tx_packets="$(awk '/Tx/ {print($3)}' clustersFlowMonitor.dat)"
		printf "%.4f," $(echo " $drops * 100 / ${tx_packets} " | bc -l ) >> $outdir/varying_buffer_drop_rate.csv
	    done
	done
tests_done="$tests_done varying_buffer"
fi

if [[ $tests == *"rtt_unfairness"* ]]; then
	echo "Running test: rtt_unfairness"
	if compgen -G "$outdir/rtt_unfair*" > /dev/null; then
		rm -r $outdir/rtt_unfair*
	fi
	long_delays="2.5 5 10 15 20 25"
	short_delay="2.5"
	for long_delay in $long_delays; do
	    printf "\n%s," ${long_delay} >> $outdir/rtt_unfairness_table.csv
	    for tcp_variant in $tcp_variants; do
		set  +e
# In Remy, RTT unfairness experiment is done with 4 longFlows with RTTs=50ms, 100ms, 150ms and 200ms respectively
		./ns3 run --no-build "scratch/clusters --transportProtocol=$tcp_variant --runtime=100 \
	          --longFlows=2 --shortFlows=0 --bottleneckLinkDataRate=100Mbps  --bottleneckLinkDelay=1ms \
	          --endNodeLinkCapacity=100Mbps --endNodeLinkDelay="${short_delay}ms,${long_delay}ms" --queueDiscType=FifoQueueDisc \
		  --queueSize=10p --tracing=false --mtu=1400 --isSack=true --traceSamplingTime=10 --numberOfPackets=0 \
		  --isDataRateVariable=false --isRandomLossVariable=false --isDelayVariable=false --numberOfCoreRouters=2 \
		  --topology=dumbbell --randomLossRate=0 --isRandomArrival=true"
		set -e
		throughput=( $(awk '/Throughput:/ {print($2)}' clustersFlowMonitor.dat) )
		if [ $(echo "${throughput[1]} >= ${throughput[0]}" | bc -l) -eq 1 ]
		then
			printf "%.2f," $(echo " ${throughput[0]} / ${throughput[1]} " | bc -l ) >> $outdir/rtt_unfairness_table.csv
		else
			printf "%.2f," $(echo " ${throughput[1]} / ${throughput[0]} " | bc -l ) >> $outdir/rtt_unfairness_table.csv
		fi
	    done
	done
tests_done="$tests_done rtt_unfairness"
fi

if [[ $tests == *"varying_bw"* ]]; then
	echo "Running test: varying_bw"
	if compgen -G "$outdir/varying_bw*" > /dev/null; then
    		rm -r $outdir/varying_bw*
	fi
	for tcp_variant in $tcp_variants; do
		set  +e
		./ns3 run --no-build "scratch/clusters --transportProtocol=$tcp_variant --runtime=100 \
	          --longFlows=1 --shortFlows=0 --bottleneckLinkDataRate=100Mbps  --bottleneckLinkDelay=15ms \
	          --endNodeLinkCapacity=0Mbps --endNodeLinkDelay=0ms --queueDiscType=FifoQueueDisc --queueSize=1p \
		  --tracing=false --mtu=1400 --isSack=true --traceSamplingTime=0.05 --numberOfPackets=0 --isDataRateVariable=true \
         	  --isRandomLossVariable=false --isDelayVariable=false --numberOfCoreRouters=2 --topology=singleLink \
		  --randomLossRate=0 --isRandomArrival=true"
		set -e
# Must include the varying link capacity from clusterLinkCapacity.csv, in the first column.<---- no two simulation runs produce the same randomness
		if [ ! -f "$outdir/varying_bw_table.csv" ]
		then
        	        mv clustersThroughput.csv $outdir/varying_bw_table.csv
		else
			mv $outdir/varying_bw_table.csv temp_table.csv
			cut -d "," -f 2 clustersThroughput.csv > temp.csv
			paste -d "" temp_table.csv temp.csv > temp_bw_table.csv
			sed 's/$/,/g' temp_bw_table.csv > $outdir/varying_bw_table.csv
	        fi
	done
tests_done="$tests_done varying_bw"
fi

if [[ $tests == *"convergence"* ]]; then
	echo "Running test: convergence"
	if compgen -G "$outdir/convergence*" > /dev/null; then
    		rm -r $outdir/convergence*
	fi
	for tcp_variant in $tcp_variants; do
		set  +e
		./ns3 run --no-build "scratch/clusters --transportProtocol=$tcp_variant --runtime=20 \
	          --longFlows=10 --shortFlows=0 --bottleneckLinkDataRate=100Mbps  --bottleneckLinkDelay=10ms \
	          --endNodeLinkCapacity=100Mbps --endNodeLinkDelay=0ms --queueDiscType=FifoQueueDisc --queueSize=100p \
		  --tracing=false --mtu=1400 --isSack=true --traceSamplingTime=0.02 --numberOfPackets=0 --isDataRateVariable=false \
		  --isRandomLossVariable=false --isDelayVariable=false --numberOfCoreRouters=2 --topology=dumbbell \
		  --randomLossRate=0 --isRandomArrival=false"
		set -e
		mv clustersThroughput.csv $outdir/convergence_${tcp_variant}.csv
		cut -d "," -f 2 clustersFairnessIndex.csv > $outdir/convergence_${tcp_variant}_cdf.csv
		#awk '{for(i=1;i<=NF;i++){sum+=$i}}END{print sum}' RS='$' FPAT='-{0,1}[0-9]+' temp.csv
		awk '{count[$1]++} END {for (number in count) sum+=count[number]; for (number in count) print number, count[number]/sum}' $outdir/convergence_${tcp_variant}_cdf.csv | sort > $outdir/convergence_${tcp_variant}_pdf.csv
	done
tests_done="$tests_done convergence"
fi

if [[ $tests == *"fairness"* ]]; then
	echo "Running test: fairness"
        if compgen -G "$outdir/fairness*" > /dev/null; then
                rm -r $outdir/fairness*
	fi
	tcpSenders=5
	linkCapacity=10Mbps
	for tcp_variant in $tcp_variants; do
		set  +e
                ./ns3 run --no-build "scratch/clusters --transportProtocol=$tcp_variant --runtime=20 \
                  --longFlows=$tcpSenders --shortFlows=0 --bottleneckLinkDataRate=$linkCapacity --bottleneckLinkDelay=10ms \
                  --endNodeLinkCapacity=10Mbps --endNodeLinkDelay=0ms --queueDiscType=FifoQueueDisc --queueSize=20p \
                  --tracing=false --mtu=1400 --isSack=true --traceSamplingTime=0.1 --numberOfPackets=0 --isDataRateVariable=false \
                  --isRandomLossVariable=false --isDelayVariable=false --numberOfCoreRouters=2 --topology=dumbbell \
                  --randomLossRate=0 --isRandomArrival=true"
                set -e
		#linkCapacity_in_Mbps=$(echo $linkCapacity | tr -d -c 0-9)
		#fairShare=$(echo "scale=2; ${linkCapacity_in_Mbps} / $tcpSenders" | bc -l)
		#printf "fairshare=%.2f\n" $(echo "scale=2; ${linkCapacity_in_Mbps} / $tcpSenders" | bc -l) >> $outdir/fairness_${tcp_variant}.csv
                #cat clustersThroughput.csv >> $outdir/fairness_${tcp_variant}.csv
		mv clustersThroughput.csv $outdir/fairness_${tcp_variant}.csv
        done
tests_done="$tests_done fairness"
fi

if [[ $tests == *"dynamics"* ]]; then
	echo "Running test: dynamics"
	if compgen -G "$outdir/dynamics*" > /dev/null; then
		rm -r $outdir/dynamics*
	fi
	for tcp_variant in $tcp_variants; do
	    for trial in {1..15}; do
		printf "\n%d," $trial >> $outdir/dynamics_${tcp_variant}_throughput.csv
		printf "\n%d," $trial >> $outdir/dynamics_${tcp_variant}_delay.csv
		set  +e
		./ns3 run --no-build "scratch/clusters --transportProtocol=$tcp_variant --runtime=100 \
	          --longFlows=8 --shortFlows=0 --bottleneckLinkDataRate=100Mbps  --bottleneckLinkDelay=15ms \
	          --endNodeLinkCapacity=100Mbps --endNodeLinkDelay=1ms --queueDiscType=FifoQueueDisc \
		  --queueSize=100p --tracing=false --mtu=1400 --isSack=true --traceSamplingTime=10 --numberOfPackets=0 \
		  --isDataRateVariable=false --isRandomLossVariable=false --isDelayVariable=false --numberOfCoreRouters=2 \
		  --topology=dumbbell --randomLossRate=0 --isRandomArrival=true --trial=$trial"
		set -e
		throughput=( $(awk '/Throughput:/ {print($2)}' clustersFlowMonitor.dat) )
		delay=( $(awk '/Mean delay:/ {print($3)}' clustersFlowMonitor.dat) )
		printf '%.2f,' "${throughput[@]}" >> $outdir/dynamics_${tcp_variant}_throughput.csv
		printf '%.2f,' "${delay[@]}" >> $outdir/dynamics_${tcp_variant}_delay.csv
	    done
	done
tests_done="$tests_done dynamics"
fi

#Looks like both convergence and jain Index are same experiments......
if [[ $tests == *"jain_fairness"* ]]; then
	echo "Running test: jain_fairness"
	if compgen -G "$outdir/jain_fairness*" > /dev/null; then
		rm -r $outdir/jain_fairness*
	fi
	for tcp_variant in $tcp_variants; do
		set  +e
		./ns3 run --no-build "scratch/clusters --transportProtocol=$tcp_variant --runtime=100 \
	          --longFlows=4 --shortFlows=0 --bottleneckLinkDataRate=100Mbps  --bottleneckLinkDelay=1ms \
	          --endNodeLinkCapacity=100Mbps --endNodeLinkDelay=1ms --queueDiscType=FifoQueueDisc \
		  --queueSize=10p --tracing=false --mtu=1400 --isSack=true --traceSamplingTime=10 --numberOfPackets=0 \
		  --isDataRateVariable=false --isRandomLossVariable=false --isDelayVariable=false --numberOfCoreRouters=2 \
		  --topology=dumbbell --randomLossRate=0 --isRandomArrival=true"
		set -e
		if [ ! -f "$outdir/jain_fairness_table.csv" ]
		then
        	        mv clustersFairnessIndex.csv $outdir/jain_fairness_table.csv
		else
			mv $outdir/jain_fairness_table.csv temp_table.csv
			cut -d "," -f 2 clustersFairnessIndex.csv > temp.csv
			paste -d "" temp_table.csv temp.csv > temp_fairness_table.csv
			sed 's/$/,/g' temp_fairness_table.csv > $outdir/jain_fairness_table.csv
	        fi
	done
tests_done="$tests_done jain_fairness"
fi

if [[ $tests == *"fct_short_flows"* ]]; then
	echo "Running test: fct_short_flows"
        if compgen -G "$outdir/fct_short_flows*" > /dev/null; then
                rm -r $outdir/fct_short_flows*
        fi
tests_done="$tests_done fct_short_flows"
fi

if [[ $tests == *"coexist"* ]]; then
        echo "Running test: coexist"
        if compgen -G "$outdir/coexist*" > /dev/null; then
                rm -r $outdir/coexist*
        fi
        for tcp_variant in $tcp_variants; do
	    if [[ $tcp_variant == "TcpCubic" ]]; then
		continue;
	    fi
	    printf "\n%s," $tcp_variant >> $outdir/coexist_vsTcpCubic.csv
	    for trial in {1..50}; do
		#printf "\n%d," $trial >> $outdir/coexist_${tcp_variant}vTcpCubic.csv
                set  +e
                ./ns3 run --no-build "scratch/clusters --transportProtocol=$tcp_variant,TcpCubic --runtime=50 \
                  --longFlows=2 --shortFlows=0 --bottleneckLinkDataRate=10Mbps  --bottleneckLinkDelay=25ms \
                  --endNodeLinkCapacity=10Mbps --endNodeLinkDelay=0ms --queueDiscType=FifoQueueDisc \
                  --queueSize=20p --tracing=false --mtu=1400 --isSack=true --traceSamplingTime=10 --numberOfPackets=0 \
                  --isDataRateVariable=false --isRandomLossVariable=false --isDelayVariable=false --numberOfCoreRouters=2 \
                  --topology=dumbbell --randomLossRate=0 --isRandomArrival=true --trial=$trial"
                set -e
		throughput=( $(awk '/Throughput:/ {print($2)}' clustersFlowMonitor.dat) )
		printf "%.4f," $(echo " ${throughput[0]} / ${throughput[1]} " | bc -l ) >> $outdir/coexist_vsTcpCubic.csv
	    done
	done
tests_done="$tests_done coexist"
fi

if [[ $tests == *"incast"* ]]; then
	echo "Running test: incast"
	if compgen -G "$outdir/incast*" > /dev/null; then
		rm -r $outdir/incast*
	fi
	senders=1
	while [ $senders -lt 35 ]
	do
	    printf "\n%d," $senders >> $outdir/incast_table.csv
	    for tcp_variant in $tcp_variants; do
		set  +e
		./ns3 run --no-build "scratch/clusters --transportProtocol=$tcp_variant --runtime=100 \
	          --longFlows=$senders --shortFlows=0 --bottleneckLinkDataRate=1Gbps  --bottleneckLinkDelay=5us \
	          --endNodeLinkCapacity=1Gbps --endNodeLinkDelay=0us --queueDiscType=FifoQueueDisc --queueSize=1p \
		  --tracing=false --mtu=1400 --isSack=true --traceSamplingTime=10 --numberOfPackets=0 --isDataRateVariable=false \
		  --isRandomLossVariable=false --isDelayVariable=false --numberOfCoreRouters=2 --topology=dumbbell \
		  --randomLossRate=0 --isRandomArrival=true"
		set -e
		goodput=( $(awk '/Goodput:/ {print($2)}' clustersFlowMonitor.dat) )
		sum=$( IFS="+"; bc <<< "${goodput[*]}" )
		printf "%.2f," $(echo " ${sum} / ${#goodput[@]} " | bc -l ) >> $outdir/incast_table.csv
	    done
	    let "senders+=3"
	done
tests_done="$tests_done incast"
fi

if compgen -G "temp*.csv" > /dev/null; then
                rm -r temp*.csv
fi
echo "done running all tests: $tests_done"
