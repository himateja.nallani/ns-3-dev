# TCP MADRAS: Learning the dynamics of our Internet
TCP congestion control, a cornerstone of modern networking, governs how data is sent across the internet. Traditional algorithms, like TCP Reno and Cubic, employ heuristic rules to adjust transmission rates, often relying on assumptions about network behavior that may not hold true in today's complex and dynamic environments. Deep Reinforcement Learning (DRL) offers a promising alternative, enabling agents to learn optimal strategies directly from network interactions.

![deep rl](images/deep_rl.jpg)

MADRAS - Multi Agent Decisions for Robust Adversarial Strategies, aims to find the best algorithm trained across different networks like wired/wireless, datacentre/wide-area, lossy/highspeed links, etc. These type of versatile networks are generated using Network Simulator (NS-3).

## Goal
The main objective of MADRAS is to learn the dynamics/physics of the Internet and produce the optimal sequences of transmissions that would result in higher throughput, lower packet loss and lower delays. So, we seek to achieve MADRAS to Internet Congestion Control, which is what AlphaGo is for the chess game.

## Installation
1. Install all required dependencies for ns-3.

| Prerequisite | Package/version |
| ------------ | --------------- |
| C++ compiler | clang++ or g++ (g++ version 9 or greater) |
| Python | python3 (version >=3.8) |
| CMake | cmake (version >=3.13) |
| Build system | make, ninja, xcodebuild (XCode) |
| Git | any recent version (to access ns-3 from GitLab.com) |

2. Install Abseil
```sh
git clone https://github.com/abseil/abseil-cpp.git
cd abseil-cpp
mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr/local -DABSL_BUILD_TESTING=ON -DABSL_USE_GOOGLETEST_HEAD=ON -DCMAKE_CXX_STANDARD=17 -DABSL_PROPAGATE_CXX_STD=ON ..
make -j 4
sudo make install
```
3. Install Protobuf
```sh
git clone https://github.com/protocolbuffers/protobuf.git
cd protobuf
git submodule update --init --recursive
mkdir build && cd build
cmake -DCMAKE_INSTALL_PREFIX=/usr/local -DCMAKE_CXX_STANDARD=17 -Dprotobuf_ABSL_PROVIDER=package -DCMAKE_PREFIX_PATH=/usr/local ..
make -j 4
sudo make install
```
4. Install gRPC
```sh
git clone --recurse-submodules -b v1.66.0 --depth 1 --shallow-submodules https://github.com/grpc/grpc
cd grpc
mkdir build && cd build
cmake -DgRPC_INSTALL=ON -DgRPC_BUILD_TESTS=OFF -DCMAKE_INSTALL_PREFIX=/usr/local -DBUILD_SHARED_LIBS=ON -DABSL_PROPAGATE_CXX_STD=ON ..
make -j 4
sudo make install
```
5. System configuration changes
```sh
# to enable sys.path with dynamically linked libraries
nano ~/.bashrc
# add the following lines at the end of the file
LD_LIBRARY_PATH=/usr/local/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH
# save and reload
source ~/.bashrc
```
6. Pip install packages
> Note: Make sure grpc and protobuf versions in both C++ and pip installations are same.

```sh
# Create a virtual environment in ns-3-dev folder
python3 -m venv .venv
source .venv/bin/activate
# Install the following packages using pip.
pip3 install torch torchvision torchaudio
pip3 install grpcio==1.66.0 grpcio-tools==1.66.0 protobuf==5.27.2
pip3 install cppyy
```
7. Clone this repository
```sh
git clone https://gitlab.com/himateja.nallani/ns-3-dev.git
```
8. Configure and build ns-3 project:
```sh
./ns3 configure --build-profile=debug --enable-examples
./ns3 build
```

## Results
![incast](images/datacentre_incast.jpg)
![convergence](images/convergence.jpg)
![fairness](images/fairness.jpg)
![distribution](images/thrpt_vs_delay.jpg)
![varying buffer](images/varying_buffer.jpg)
![varying bandwidth](images/varying_bandwidth.jpg)
![rtt unfairness](images/rtt_unfairness.jpg)
![random losses](images/random_pkt_loss.jpg)
![tcp friendliness](images/friendliness.jpg)
![jain fairness](images/jain_fairness.jpg)
![jain index](images/jain_index.jpg)

## License
Copyright @IITMadras

