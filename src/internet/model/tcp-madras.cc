/*
 * Copyright (c) 2024 IIT Madras
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Hima Teja Nallani <himateja.nallani@gmail.com>
 */
#define NS_LOG_APPEND_CONTEXT                                                                      \
	if (m_node)										   											   \
    {                                                                                              \
        std::clog << Simulator::Now().GetSeconds() << " " << " [node " << m_node->GetId() << "] "; \
    }

#include "ns3/core-module.h"
#include "ns3/tcp-madras.h"
#include "ns3/log.h"
#include "ns3/simulator.h"
#include "ns3/config.h"

#include <grpcpp/grpcpp.h>
#include <sys/types.h>
#include <signal.h>
#include <sys/wait.h>

#define MSGSIZE 16

namespace ns3 {

NS_LOG_COMPONENT_DEFINE ("TcpMadras");
NS_OBJECT_ENSURE_REGISTERED (TcpMadras);

TypeId
TcpMadras::GetTypeId (void)
{
	static TypeId tid = TypeId ("ns3::TcpMadras")
    	.SetParent<TcpCongestionOps> ()
		.AddConstructor<TcpMadras> ()
		.SetGroupName ("Internet")
        .AddAttribute("AgentMap", "unique number of agents in this simulation run",
                      StringValue("None:None"),
					  MakeStringAccessor(&TcpMadras::m_agentMap),
					  MakeStringChecker())
	;
	return tid;
}

TcpMadras::TcpMadras ()
: TcpCongestionOps ()
{
	NS_LOG_FUNCTION (this << "Process ID:" << getpid());
	m_randomVariable = CreateObject<UniformRandomVariable> ();

	if(!m_hasStubBeenCreated)
	{
		/*
		 * Generate a random number between [49152, 65535] to be assigned to communicate with tcp_madras.py script.
		 */
		NS_ASSERT_MSG(m_randomVariable, "TcpMadras: Random Variable is null.");
		m_port = m_randomVariable->GetInteger(49152, 65535);

		m_channel = grpc::CreateChannel("localhost:"+std::to_string(m_port), grpc::InsecureChannelCredentials());
		m_stub = Communicate::NewStub(m_channel);
		m_hasStubBeenCreated = true;

		/*
		 * Establishing communication pipes, pipe p[0] for reading end, p[1] for writing end.
		 */
		int pipeToPy[2];
		if (pipe(pipeToPy) < 0){
			NS_FATAL_ERROR("Couldn't open pipe between two threads");
		}
		/*
		 * Creating a subprocess to run python code.
		 * Forking creates the exact copy of the parent process.
		 * returns the child process’s PID (a positive number) to the parent.
		 */
		m_pid = fork();

		if(m_pid == 0)
		{
			/* This is a child thread, getppid() will give parent process id, and getpid() will give its own process id.
			 * Also, Python interpreter's process id is the same as this child's process pid.
			 */
			/*
			PyStatus status; PyConfig config;
			PyConfig_InitPythonConfig(&config);
			config.verbose = 0;
			status = Py_InitializeFromConfig(&config);
			*/
			Py_Initialize();
			m_save = PyEval_SaveThread(); //Save this python thread state in a local variable, so as to restore it in destructor.

			StartPythonScript(pipeToPy[0], pipeToPy[1]);
			Py_Finalize();

			// Stop this forked(duplicate) thread to continue its simulation run
			exit(0);
		}
		else if(m_pid > 0){
			/*
			 * This is a parent thread, m_pid is the child thread's process id. We use read and write pipes to communicate between
			 * python interpreter(child) and C++(parent) program to let child inform parent when to continue its run.
			 */
			char buffer[MSGSIZE] = "";

			close(pipeToPy[1]);	// Close writing end of this pipe, and wait for reading end only.
			read(pipeToPy[0], buffer, MSGSIZE);
			close(pipeToPy[0]);
		}
		else if(m_pid < 0)
		{
			NS_LOG_ERROR("Forking failed");
		}
	}
}

TcpMadras::TcpMadras (const TcpMadras &sock)
: TcpCongestionOps (sock),
  m_maxBwFilter(sock.m_maxBwFilter),
  m_bandwidthWindowLength(sock.m_bandwidthWindowLength),
  m_minRTTFilter(sock.m_minRTTFilter),
  m_rttWindowLength(sock.m_rttWindowLength),
  //m_targetCWnd(sock.m_targetCWnd),
  m_pacingGain(sock.m_pacingGain),
  m_cWndGain(sock.m_cWndGain),
  m_hasSeenRtt(sock.m_hasSeenRtt),
  m_priorSentTime(sock.m_priorSentTime),
  m_interSendingTime(sock.m_interSendingTime),
  m_priorArrivalTime(sock.m_priorArrivalTime),
  m_interArrivalTime(sock.m_interArrivalTime),
  m_randomVariable(sock.m_randomVariable),
  m_hasStubBeenCreated(sock.m_hasStubBeenCreated),
  m_pid(sock.m_pid),
  m_port(sock.m_port),
  m_agentMap(sock.m_agentMap)
{
	NS_LOG_FUNCTION (this);
}

TcpMadras::~TcpMadras (void)
{
	/*
	 * Condition on whether gRPC port is active or not. If yes then send a SIGTERM signal to child process
	 * where the python captures it and gracefully shutdowns
	 */
	NS_LOG_FUNCTION (this << "Process ID:" << getpid() << " with Child id:"  << m_pid
					<< " for corresponding port:" << m_port);
	if(m_channel != NULL){
		kill(m_pid, SIGTERM);
		int status;
		pid_t result = waitpid(m_pid, &status, /*WNOHANG*/0);
		NS_LOG_DEBUG ("Program terminated with process id:" << result);
	}
}

void
TcpMadras::StartPythonScript(int readPipe, int writePipe)
{
	NS_LOG_FUNCTION (this << "Starting the python script by using embedded python");
	std::string port = std::to_string(m_port);
	/*
	 * Send simulator duration time (using EventId) to terminate the python process.
	 * Time GetDelayLeft(const EventId& id);
	 */
	/*
	EventId eventID = Simulator::GetStopEvent();
	double simulationTime = Simulator::GetDelayLeft(eventID).GetSeconds();
	std::string simulatorTime = std::to_string(simulationTime);
	 */
	std::string readFileDescriptor = std::to_string(readPipe);
	std::string writeFileDescriptor = std::to_string(writePipe);
	//std::string agentId = "1"/*std::to_string(m_randomVariable->GetInteger(0, m_agents))*/;

	PyEval_RestoreThread(m_save);
	PyGILState_STATE gilState = PyGILState_Ensure();	// Get the GIL

	PyRun_SimpleString("import os, sys");
	PyRun_SimpleString("sys.path.append(os.getcwd())");
	/*
	PyRun_SimpleString("print(sys.path)");
	PyRun_SimpleString("print(sys.prefix)");
	PyRun_SimpleString("print(sys.executable)");
	PyRun_SimpleString("import importlib.machinery");
	PyRun_SimpleString("print(importlib.machinery.all_suffixes())");
	*/
	PyRun_SimpleString("from src.internet.model import tcp_madras");
	PyRun_SimpleString(("tcp_madras.serve("+ port + "," /*+ agentId + ","*/
						+ readFileDescriptor + "," + writeFileDescriptor + ")").c_str());

	PyGILState_Release(gilState);	// Release the GIL

}

std::string
TcpMadras::GetName () const
{
	NS_LOG_FUNCTION (this);
	return "TcpMadras";
}

const char* const
TcpMadras::MadrasModeName[DEFAULT + 1] =
{
	"STARTUP", /*"ESTIMATE", "COMPETITIVE",*/ "DEFAULT"
};

void
TcpMadras::UpdateControlParameters (Ptr<TcpSocketState> tcb, const TcpRateOps::TcpRateConnection &rc, const TcpRateOps::TcpRateSample &rs)
{
	NS_LOG_FUNCTION (this << tcb << rs);
	SetCwnd (tcb, rs);
	SavePacketLossInfo (tcb, rc, rs);
	//SetPacingRate(tcb, m_pacingGain);
}

void
TcpMadras::SetCwnd(Ptr<TcpSocketState> tcb, const TcpRateOps::TcpRateSample &rs)
{
	NS_LOG_FUNCTION (this << tcb << rs);

	//double min = *std::min_element(x.begin(), x.end());
	//double max = *std::max_element(x.begin(), x.end());

	//int argMin = std::distance(x.begin(), std::min_element(x.begin(), x.end()));
	//int argMax = std::distance(x.begin(), std::max_element(x.begin(), x.end()));

	if(m_actions.velocity() > 0){
		uint32_t velocity_to_cWnd = m_actions.velocity() * rs.m_interval.GetSeconds();
		tcb->m_cWnd = std::max((uint32_t)1, velocity_to_cWnd % tcb->m_segmentSize) * tcb->m_segmentSize;
	}
}

void
TcpMadras::SetPacingRate(Ptr<TcpSocketState> tcb, double pacingGain)
{
	NS_LOG_FUNCTION (this << tcb << pacingGain);
	//DataRate rate (pacingGain * m_maxBwFilter.GetBest ().GetBitRate ());
	DataRate rate (pacingGain * 800000);
	rate = std::min (rate, tcb->m_maxPacingRate);	//tcb->m_maxPacingRate = 4Gbps

	if (!tcb->m_pacing)
	{
		NS_LOG_WARN ("Madras must use pacing");
		tcb->m_pacing = true;
	}
	if (tcb->m_minRtt != Time::Max ())
	{
		tcb->m_pacingRate = rate;
	}
}

void
TcpMadras::SavePacketLossInfo(Ptr<TcpSocketState> tcb, const TcpRateOps::TcpRateConnection &rc, const TcpRateOps::TcpRateSample &rs)
{
	NS_LOG_FUNCTION (this);
	if(rs.m_bytesLoss > 0){
		m_totalBytesLost += rs.m_bytesLoss;
		m_pktLossRate = m_totalBytesLost / rc.m_delivered;
	}
}

void
TcpMadras::SetMadrasState (MadrasState_t mode)
{
	NS_LOG_FUNCTION (this << mode);
	NS_LOG_DEBUG (Simulator::Now () << " Changing from " << MadrasModeName[m_state] << " to " << MadrasModeName[mode]);
	m_state = mode;
}

uint32_t
TcpMadras::GetMadrasState ()
{
	NS_LOG_FUNCTION (this);
	return m_state;
}

void
TcpMadras::EnterStartup ()
{
	NS_LOG_FUNCTION (this);
	SetMadrasState (MadrasState_t::STARTUP);
	m_pacingGain = 1;
	m_cWndGain = 1;
}

void
TcpMadras::ExtractAgentId ()
{
	NS_LOG_FUNCTION (this);
	// Extract agent id from the string map for this corresponding node
	std::stringstream splitMap(m_agentMap);
	std::string nodeToAgent;

	while(getline(splitMap, nodeToAgent, ','))
	{
		std::string node = nodeToAgent.substr(0, nodeToAgent.find(':'));

		if(node == std::to_string(m_node->GetId()))
		{
			m_agentId = nodeToAgent.substr(2, nodeToAgent.length());
			break;
		}
	}
	m_extractedAgentId = true;
}
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//----------------------------------------------Congestion Ops methods implementation---------------------------------
//____________________________________________________________________________________________________________________
uint32_t
TcpMadras::GetSsThresh (Ptr<const TcpSocketState> tcb, uint32_t bytesInFlight)
{
	NS_LOG_FUNCTION (this << tcb << bytesInFlight);
	/*
	 * This is called when we saw 3-duplicate ACKs are received, suggesting packet loss.
	 * But since we are handling it in a different way, this method is obsolete.
	 */
	NS_LOG_DEBUG ("Pkt drop recognized");
	return tcb->m_ssThresh;
}

void
TcpMadras::CongestionStateSet (Ptr<TcpSocketState> tcb, const TcpSocketState::TcpCongState_t newState)
{
	/*
	 * This is the first method that triggers when the SYN-ACK is received which was sent from the receiver.
	 */
	NS_LOG_FUNCTION (this << tcb << newState);
	if (newState == TcpSocketState::CA_OPEN)
	{
		if(!m_hasSeenRtt){
			NS_LOG_DEBUG ("First packet is triggered after SYN-ACK exchange");
			m_hasSeenRtt = true;
			//EnterStartup ();
		}
	}
}

void
TcpMadras::CwndEvent (Ptr<TcpSocketState> tcb, const TcpSocketState::TcpCAEvent_t event)
{
	NS_LOG_FUNCTION (this << tcb << event);
}

bool
TcpMadras::HasCongControl () const
{
	NS_LOG_FUNCTION (this);
	/*
	 * This method HasCongControl() is succeeded by CongControl(tcb, rc, rs)
	 * NOTE: If this returns true then setting cWnd and/or setting pacing rate is done in CongControl(tcb, rc, rs) method,
	 * 		 instead of PktsAcked() and IncreaseWindow() methods.
	 */
	return true;
}

void
TcpMadras::CongControl (Ptr<TcpSocketState> tcb, const TcpRateOps::TcpRateConnection &rc, const TcpRateOps::TcpRateSample &rs)
{
	//Implement schedule-check of Ack arrival based on some distribution of inter arrival time
	// Take two averages: windowed average and long running average fo RTT and Pkt loss. Take these mean values as t* and p* i.e., fixed values.

	/* ----------The below are the observables--------------------
	 * for rtt for every packet received 		= tcb->m_lastRtt
	 * for minimum rtt over entire connection 	= tcb->m_minRtt
	 * inflight data until this ACK 			= tcb->m_bytesInFlight
	 *
	 * packet.receivedTime or now 				= rc.m_deliveredTime
	 * packet.sentTime (for this ACK received) 	= rc.m_firstSentTime
	 * data delivered (excluding retransmit)	= rc.m_txItemDelivered
	 * data delivered (including retransmit)	= rc.m_delivered
	 *
	 * 				rs.m_interval = duration in which the following measurements were taken
	 * valid ack received (duplicate or not)	= rs.m_ackedSacked
	 * measured network bandwidth				= rs.m_deliveryRate = rs.m_delivered/rs.m_interval
	 * Bytes lost in this time interval			= rs.m_bytesLoss //recorded between m_deliveredTime and m_firstSentTime
	 */
	NS_LOG_FUNCTION (this << tcb << rc << rs);

	if(/*rc.m_delivered*/rs.m_ackedSacked > 0)
	{
		NS_LOG_DEBUG(rc.m_txItemDelivered << "/" << rc.m_delivered << "," << rs.m_delivered << "/" << rc.m_rateDelivered << "||" <<
					rc.m_firstSentTime.GetSeconds() << "-->" << rc.m_deliveredTime.GetSeconds() << "==" << rs.m_interval.GetSeconds() << "/" << rc.m_rateInterval.GetSeconds() << "||" <<
					rs.m_deliveryRate.GetBitRate() << "||" <<
					rs.m_sendElapsed.GetSeconds() << "," << rs.m_ackElapsed.GetSeconds() << "||" <<
					rs.m_bytesLoss);
		if(!m_extractedAgentId){
			ExtractAgentId();
		}
		m_actions = NotifyToPython(tcb, rs);
		UpdateControlParameters (tcb, rc, rs);
	}
}

Ptr<TcpCongestionOps>
TcpMadras::Fork (void)
{
	NS_LOG_FUNCTION (this);
	return CopyObject<TcpMadras> (this);
}

Actions
TcpMadras::NotifyToPython (Ptr<TcpSocketState> tcb, const TcpRateOps::TcpRateSample &rs)
{
	NS_LOG_FUNCTION (this);
	Feature* feature = new Feature();
	feature->set_cwnd((int32_t)tcb->m_cWnd.Get());
	feature->set_pkt_loss((double)m_pktLossRate);
	feature->set_rtt((float)tcb->m_lastRtt.Get().GetSeconds());

	State state;
	state.set_info("");
	state.set_agent_id(m_agentId);
	/*
	 * TODO: [Reward Shaping]
	 * 1. Sending rate should be defined and set below instead of goodput
	 * 2. If jitter is too much then coefficient before log(Tr) be closer to 1;
	 * 		similarly when pkt loss rate oscillates too much then coefficient before log(Lr) should be closer to 1.
	 * 3. For cooperative multiagent games, there should be a shared reward.
	 */
	state.set_reward(log10(rs.m_deliveryRate.GetBitRate()) /*- 0.1*log10(rs.m_bytesLoss)*/ - 0.9*log10(tcb->m_lastRtt.Get().GetSeconds()));
	state.set_allocated_observation(feature);

	Actions actions;

	grpc::ClientContext context;
	NS_ASSERT_MSG(m_stub, "TcpMadras: Stub is null.");
	grpc::Status status = m_stub->Notify(&context, state, &actions);

	if (status.ok()) {
		NS_LOG_DEBUG("velocity:" << actions.velocity() << ", accelerate:" << actions.acceleration());
	} else {
		NS_LOG_ERROR(status.error_code() << ": " << status.error_message());
	}
	return actions;
}
} // namespace ns3
