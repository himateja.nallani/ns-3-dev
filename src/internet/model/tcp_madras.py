#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Copyright 2024 IITM authors.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
"""The Python implementation of the TCP Multi Agent Decisions with Robust Actor-critic Strategies (MADRAS)"""
__author__ = 'himateja.nallani@gmail.com (Himateja Nallani)'

import os, sys
'''sys.path.insert(1, "/".join(os.path.realpath(__file__).split("/")[0:-4])+"/build/bindings/python")
sys.path.insert(1, "/".join(os.path.realpath(__file__).split("/")[0:-4])+"/build/lib")
try:
    from ns import ns
except ModuleNotFoundError:
    raise SystemExit(
        "Error: ns3 Python module not found;"
        " Python bindings may not be enabled"
        " or your PYTHONPATH might not be properly configured"
    )
ns.core.NS_LOG_COMPONENT_DEFINE ("TcpMadras")'''

from concurrent import futures
#import logging

import grpc

sys.path.insert(1, "/".join(os.path.realpath(__file__).split("/")[0:-4])+"/build/include/ns3")
import madras_interface_pb2
import madras_interface_pb2_grpc

import torch as T
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import numpy as np

import argparse
import multiprocessing
import signal

_PROCESS_COUNT = multiprocessing.cpu_count()
_THREAD_CONCURRENCY = _PROCESS_COUNT

parser = argparse.ArgumentParser(description='Start simulation script on/off')
parser.add_argument('--port',
                    type=int,
                    default=50051,
                    help='localhost port number, Default: 50051')
args = parser.parse_args()
port = int(args.port)

server = None
_IS_AGENT_LOADED = False

''' ----------------Implementation of Actor Critic Continuous ------------------
This is a general deep neural network with two hidden layers for both actor network and critic network.
The difference is that the actor network will have two outputs(mu and sigma of Normal distribution),
whereas critic network has only one output v(s) = max[a] Q(s,a) i.e., estimate value function for that particular state considering we take max action.

Remember, we use neural networks as a substitute for function approximation v(s;W) and pi(a|s;THETA) and learn the parameters W and THETA.
TODO: 1) Replay Buffer: A Batch Reinforcement learning method for estimating v(s) efficiently --why--> for stablility
         And, also for trajectory estimation we need non-Markovian way to store previous windowed intervals.
         Look at PPO algorithm for minimal KL divergence (difference between two policies) and more stability.
      2) Instantiate: Initialize the weights to some initial values or RANDOM, and biases too, so that we can observe some set functionality.
      3) Steps: Update v(s;W) for every ack received, but choose_actions only at fixed time steps.
      4) Belief State: use RNN(LSTM) to generate belief states based on (prev_state, observation) tuple.
      5) TD(lambda): Multi-step returns can be used instead of single step TD update or Monte Carlo where to get G_t we need to wait till the end.
                  Because, this gives a good balance between bias-variance tradeoff.
'''
class GenericNetwork(nn.Module):

    # input -> linear -> relu -> linear -> relu -> output
    def __init__(self, learning_rate, input_dims, fc1_dims, fc2_dims, output_dims):
        # allowing child class to refer to its parent class nn.Module
        super(GenericNetwork, self).__init__()

        self.learning_rate = learning_rate
        self.input_dims = input_dims
        self.fc1_dims = fc1_dims
        self.fc2_dims = fc2_dims
        self.output_dims = output_dims

        # form a two layer fully connected layers
        self.fc1 = nn.Linear(*self.input_dims, self.fc1_dims)
        self.fc2 = nn.Linear(self.fc1_dims, self.fc2_dims)

        self.pi = nn.Linear(self.fc2_dims, self.output_dims)
        self.v = nn.Linear(self.fc2_dims, 1)

        self.optimizer = optim.Adam(self.parameters(), lr = self.learning_rate)

        # If GPU is available
        self.device = T.device('cuda:0' if T.cuda.is_available() else 'cpu')
        #print(f"Using {self.device} device")
        self.to(self.device)

    def forward(self, observation:'list'):
        # convert input list to a tensor
        state = T.tensor(observation, dtype=T.float).to(self.device)

        x = F.relu(self.fc1(state))
        x = F.relu(self.fc2(x))

        # There is no activation function in the last layer, we want to handle it by our own say [-1,1] or [0,inf]
        # A neural network without an activation function is essentially just a linear regression.
        pi = self.pi(x)
        v = self.v(x)

        return (pi, v)

class Agent(object):
    # alpha = learning rate for actor network, beta = learning rate for critic network
    def __init__(self, alpha, beta, input_list:'list', output_list:'list', gamma=0.99, layer1_size=64, layer2_size=64):
        self.gamma = gamma
        self.log_probability = None
        self.input_list = input_list
        self.output_list = output_list

        # For n outputs, mu is a nX1 vector and covariance is a nXn matrix
        mu_vector_size = len(output_list)
        sigma_matrix_size = len(output_list)**2

        # initialize mu vector and sigma matrix
        self.mu = np.zeros((mu_vector_size, 1))
        self.sigma = np.zeros((len(output_list), len(output_list)))

        # initialize the neural network for actor and critic separately
        self.model = GenericNetwork(alpha, [len(input_list)], layer1_size, layer2_size, output_dims=mu_vector_size+sigma_matrix_size)
        # Don't load the agent until we retrieve the agent id from C++ file
        self.PATH = None
        self.agent_id = None

    def load_agent(self, agent_id=None) -> None:
        self.agent_id = agent_id
        # load the weights from this agent's training so far
        self.PATH = "model_" + self.agent_id + ".pth"
        # check if the file exists to be loaded
        if os.path.isfile(self.PATH):
            self.model.load_state_dict(T.load(self.PATH, weights_only=True))
            self.model.eval()
            #print("[Py]Loading the agent with id:", self.agent_id)

            self.model.to(self.model.device)

    def choose_action(self, observation_map:'dict') -> 'dict':
        observation = []
        for (index,key) in enumerate(self.input_list):
            observation.insert(index, observation_map[key])

        # a flat tensor with all the output values
        y, _ = self.model.forward(observation)

        ''' NOTE: After detach() the subsequent lines of code won't affect backpropagating gradients of nn '''
        # reshaping the above flat tensor into [mu | sigma] rectangular matrix
        output_matrix = y.reshape([len(self.output_list), int(y.size(dim=-1)/len(self.output_list))]).cpu().detach().numpy()

        # designate mu vector and sigma matrix
        self.mu = T.from_numpy(output_matrix[:,0])
        self.sigma = T.from_numpy(np.delete(output_matrix, [0], axis=1))

        '''TODO: Impose conditions on mu such that velocity is a softplus/relu activation'''
        # Make it symmetric matrix (A^T)A
        self.sigma = T.mm(self.sigma, self.sigma.t())

        # Make it a positive definite matrix by adding identity matrix
        self.sigma.add_(T.eye(len(self.output_list)))

        pdf = T.distributions.multivariate_normal.MultivariateNormal(loc = self.mu, covariance_matrix = self.sigma)

        # sample X ~ N(mu,sigma) --why--> explore-exploit dilemma is solved by non-deterministic action selection.
        probability = pdf.sample()
        # attach to device to be used for loss gradient during backpropagation
        self.log_probability = pdf.log_prob(probability).to(self.model.device)

        ''' Search for a function that suits our requirement for action '''
        #probability = T.tanh(probability)

        # return the action dictionary with corresponding indices
        action = {}
        for (index,key) in enumerate(self.output_list):
            action[key] = probability.tolist()[index]

        return action

    ''' TODO: Must include action to learn for Q-values i.e., (state,action) pair instead of v(s) i.e., state alone '''
    def learn(self, state_map:'dict', reward:'float', new_state_map:'dict', done) -> None:
        # Transferring a dict to an array, with corresponding indices
        state = []
        new_state = []
        for (index,key) in enumerate(self.input_list):
            state.insert(index, state_map[key])
            new_state.insert(index, new_state_map[key])

        # zeroes the gradient buffers of all parameters, else gradients will be accumulated
        self.model.optimizer.zero_grad()

        # v(s) = max[action] {Q(s,a)}
        ''' Note: The below is v(s) instead of Q(s,a) because we are not inputing any action
            like self.critic.forward(new_state, self.actor.action) '''
        ''' TODO: we can use Q_max(s,a) = T.reduce_max(Q_new) '''
        _, value_fn_new = self.model.forward(new_state)
        _, value_fn = self.model.forward(state)

        # TD(0) update for weights
        reward = T.tensor(reward, dtype=T.float).to(self.model.device)
        delta = (reward + self.gamma * value_fn_new * (1-int(done))) - value_fn

        # loss formulations for both networks
        ''' TODO: Add regularization term such as lasso, to ensure the weights are not wildly changed '''
        actor_loss = -self.log_probability * delta
        critic_loss = T.square(delta)

        # perform backpropagation inorder to update parameters w, theta
        (actor_loss + critic_loss).backward()

        # Does the update
        self.model.optimizer.step()

    def __del__(self):
        # save the agent's weights using it's agent_id
        if self.agent_id is not None:
            #print("Saving the agent with id:", self.agent_id)
            T.save(self.model.state_dict(), self.PATH)

class my_dict(dict):
    # This class is meant to round off a value to a key in a dictionary to 2 decimal places.
    def __str__(self):
        return str({k:round(v,2) if isinstance(v,float) else v for k,v in self.items()})

class TcpMadras(madras_interface_pb2_grpc.CommunicateServicer):

    def __init__(self):
        #super(TcpMadras, self).__init__()

        self.state_space = list()
        self.action_space = list()
        self.state = dict()
        self.action = dict()

        for each_feature in madras_interface_pb2.Feature.DESCRIPTOR.fields:
            self.state_space.append(each_feature.name)
            self.state[each_feature.name] = 0

        for each_action in madras_interface_pb2.Actions.DESCRIPTOR.fields:
            self.action_space.append(each_action.name)
            self.action[each_action.name] = 0

        # creating an object for Agent class
        self.agent = Agent(alpha=0.000005, beta=0.00001, input_list=self.state_space, output_list=self.action_space,
                           gamma=0.9, layer1_size=64, layer2_size=64)

        signal.signal(signal.SIGTERM, self.exit_gracefully)

    def exit_gracefully(self, signum, frame):
        # explicitly stopping the server so that control returns back to c++ for this child thread.
        server.stop(grace=None)
        # explicitly delete the Agent object so we can save the trained model
        del self.agent
        return

    # NOTE: request = <google.protobuf.message.Message object>
    # DESCRIPTOR = <google.protobuf.descriptor.Descriptor object>
    def Notify(self, request, context):
        # load TcpMadras agent with previously trained parameters
        global _IS_AGENT_LOADED
        if (_IS_AGENT_LOADED is False) and (request.agent_id is not None):
            self.agent.load_agent(request.agent_id)
            _IS_AGENT_LOADED = True

        observation_new = request.observation.DESCRIPTOR.fields_by_name  # returns dict(str, FieldDescriptor)

        state_new = {}
        reward = request.reward
        for (field_name, field_descriptor) in observation_new.items(): # dict.items() returns a tuple for each key
            if field_descriptor.message_type:
                # Composite field
                print("The field:", field_name, "has a composite field:", field_descriptor.message_type.name,
                      "Please simplify the datatype to either int or float.")
            else:
                # Raw field
                state_new[field_name] = getattr(request.observation, field_name)

        # retrieve the action selected by the neural network
        action = self.agent.choose_action(state_new)
        self.agent.learn(self.state, reward, state_new, done=0)
        #ns.core.NS_LOG_DEBUG(f"state:{state_new}, reward:{reward} and action:{action}")
        #print("state:", my_dict(state_new), "action:", my_dict(action), "and reward:%.2f" %(reward))
        self.state = state_new
        #score += reward

        action_object = madras_interface_pb2.Actions()
        for (field_name, field_descriptor) in action_object.DESCRIPTOR.fields_by_name.items():
            setattr(action_object, field_name, action[field_name])
        return action_object

def serve(port, read_pipe=None, write_pipe=None):
    # ensure our global variables gets assigned
    global server
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=_THREAD_CONCURRENCY))

    madras_interface_pb2_grpc.add_CommunicateServicer_to_server(servicer=TcpMadras(), server=server)
    server.add_insecure_port("[::]:" + str(port))
    server.start()
    try:
        # Both read_pipe, write_pipe are in strings, to be converted to int to be used as file descriptors.
        os.close(int(read_pipe))
        # use pipe() to communicate to c++ code.
        os.write(int(write_pipe), b"port listens")
        os.close(int(write_pipe))
    except TypeError:
        pass

    #ns.core.NS_LOG_FUNCTION("[Py]gRPC listens on port:", port, "with agent id:", _AGENT_ID)
    #print("[Py]gRPC listens on port:", port)
    # timeout: A floating point number specifying a timeout for the operation in seconds.
    # Instead of None, we must give the simulator duration, to make sure threads don't hang forever.
    server.wait_for_termination(timeout=None)
    #ns.core.NS_LOG_FUNCTION("[Py]Stopped listening on port:", port)
    #print("[Py]Stopped listening on port:", port)
    return

'''
if __name__ == "__main__":
    logging.basicConfig()
    serve(port)
'''
