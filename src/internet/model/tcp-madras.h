/*
 * Copyright (c) 2024 IIT Madras
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Authors: Hima Teja Nallani <himateja.nallani@gmail.com>
 */

#ifndef TCPMADRAS_H
#define TCPMADRAS_H

#include "ns3/tcp-congestion-ops.h"
#include "windowed-filter.h"
//#include "ns3/tcp-socket-base.h"
#include "ns3/madras-interface.pb.h"
#include "ns3/madras-interface.grpc.pb.h"
#include <unistd.h>

#define PY_SSIZE_T_CLEAN
#include <Python.h>

namespace ns3 {

class TcpMadras : public TcpCongestionOps
{
public:
	/**
	 * \brief Get the type ID.
	 * \return the object TypeId
	 */
	static TypeId GetTypeId (void);

	/**
	 * \brief Constructor
	 */
	TcpMadras ();

	/**
	 * Copy constructor.
	 * \param sock The socket to copy from.
	 */
	TcpMadras (const TcpMadras &sock);

	~TcpMadras ();

	typedef enum
	{
		STARTUP,	/* This refers to the start of a TCP connection, where we incrementally move towards equilibrium*/
		DEFAULT,	/* Where Actor-Critic operates without any restrictions */
	} MadrasState_t;

	typedef enum {
		CONSERVATIVE,
		AGGRESSIVE,
		VISIT_ALL_STATES,
	} MadrasPolicy_t;

	static const char* const MadrasModeName[DEFAULT + 1];

	typedef WindowedFilter<DataRate, MaxFilter<DataRate>, uint32_t, uint32_t> MaxBandwidthFilter;
	typedef WindowedFilter<Time, MinFilter<Time>, uint64_t, uint64_t> MinRTTFilter;

	std::string GetName () const;

	virtual uint32_t GetSsThresh (Ptr<const TcpSocketState> tcb, uint32_t bytesInFlight);
	virtual void CongestionStateSet (Ptr<TcpSocketState> tcb, const TcpSocketState::TcpCongState_t newState);
	virtual void CwndEvent (Ptr<TcpSocketState> tcb, const TcpSocketState::TcpCAEvent_t event);
	virtual bool HasCongControl () const;
	virtual void CongControl (Ptr<TcpSocketState> tcb, const TcpRateOps::TcpRateConnection &rc, const TcpRateOps::TcpRateSample &rs);
	virtual Ptr<TcpCongestionOps> Fork ();

private:
	void EnterStartup(void);
	void SetMadrasState (MadrasState_t mode);
	uint32_t GetMadrasState ();
	void UpdateControlParameters (Ptr<TcpSocketState> tcb, const TcpRateOps::TcpRateConnection &rc, const TcpRateOps::TcpRateSample &rs);
	void SetCwnd (Ptr<TcpSocketState> tcb, const TcpRateOps::TcpRateSample &rs);
	void SetPacingRate(Ptr<TcpSocketState> tcb, double pacingGain);
	void SavePacketLossInfo(Ptr<TcpSocketState> tcb, const TcpRateOps::TcpRateConnection &rc, const TcpRateOps::TcpRateSample &rs);

	double GetAction(std::vector<double> stateVector);
	Actions NotifyToPython(Ptr<TcpSocketState> tcb, const TcpRateOps::TcpRateSample &rs);
	void StartPythonScript(int readPipe, int writePipe);
	void ExtractAgentId(void);

private:
	std::shared_ptr<grpc::Channel> m_channel;
	std::unique_ptr<Communicate::Stub> m_stub;
	PyThreadState* m_save;
	Actions m_actions;

	MadrasState_t   		m_state        					{MadrasState_t::DEFAULT};
	MaxBandwidthFilter   	m_maxBwFilter;
	uint32_t    			m_bandwidthWindowLength       	{0};
	MinRTTFilter 			m_minRTTFilter;
	uint32_t    			m_rttWindowLength       		{0};
	//uint32_t    			m_targetCWnd                  	{0};
	double      			m_pacingGain                  	{1};
	double 					m_cWndGain						{1};
	bool        			m_hasSeenRtt                  	{false};
	Time					m_priorSentTime					{Time::Max()};
	Time					m_interSendingTime				{Time::Max()};
	Time 					m_priorArrivalTime				{Time::Max()};
	Time					m_interArrivalTime				{Time::Max()};
	Ptr<UniformRandomVariable> m_randomVariable				{nullptr};
	bool					m_hasStubBeenCreated			{false};
	pid_t 					m_pid 							{-1};
	uint32_t 				m_port							{50051};
	uint32_t 				m_totalBytesLost				{0};
	double					m_pktLossRate					{0};
	std::string 			m_agentMap;
	std::string				m_agentId;
	bool					m_extractedAgentId				{false};
};

} // namespace ns3
#endif // TCPMADRAS_H
