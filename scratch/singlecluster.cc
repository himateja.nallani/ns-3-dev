/*
 * cluster.cc
 *
 *  Created on: 30-Jun-2019
 *C
 *  Last Modified on: 16-July-2021
 *      Author: hima
 */
#include <iostream>
#include <fstream>
#include <string>
#include <iterator> // for iterators
#include <random>
#include <map>
#include <utility>
#include <queue>
#include <deque>
#include "ns3/point-to-point-dumbbell.h"
#include "ns3/netanim-module.h"
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/traffic-control-module.h"
#include "ns3/flow-monitor-module.h"

using namespace ns3;
using namespace std;

static string fileName = "singlecluster";
static uint32_t bytesInLink = 0;
Time previousTime = Seconds (0);
static map<uint32_t, uint32_t> cwnd, /*rtt,*/ previousAckCount, softwareQueueValue, droppedBytesInBuffer;
static map<uint32_t, uint64_t> previousTxBytes, previousDelaySum;
static bool firstTracePacketsInLink = true;
//static vector<bool> firstTraceRouterBufferLosses =true;
static Ptr<OutputStreamWrapper> streamSoftwareQueue, streamLinkUtilization, streamLinkDrops, streamWindow, streamRTT, streamFlowMonitor, streamTxData, streamAvgThroughput, streamThroughput, /*streamFairness,*/ streamLinkCapacity;
static DataRate bottleneckLinkCapacity;

NS_LOG_COMPONENT_DEFINE(fileName);

class MyApp : public Application
{
public:
	MyApp ();
	virtual ~MyApp ();

	/**
	 * Register this type.
	 * \return The TypeId.
	 */
	static TypeId GetTypeId (void);
	void Setup (Ptr<Socket> socket, Address address, uint32_t packetSize, uint32_t nPackets, DataRate dataRate);

private:
	virtual void StartApplication (void);
	virtual void StopApplication (void);

	void ScheduleTx (void);
	void SendPacket (void);

	Ptr<Socket>     m_socket;
	Address         m_peer;
	uint32_t        m_packetSize;
	uint32_t        m_nPackets;
	DataRate        m_dataRate;
	EventId         m_sendEvent;
	bool            m_running;
	uint32_t        m_packetsSent;
};

MyApp::MyApp ()
: m_socket (0),
  m_peer (),
  m_packetSize (0),
  m_nPackets (0),
  m_dataRate (0),
  m_sendEvent (),
  m_running (false),
  m_packetsSent (0)
{
}

MyApp::~MyApp ()
{
	m_socket = 0;
}

TypeId MyApp::GetTypeId (void)
{
	static TypeId tid = TypeId ("MyApp").SetParent<Application> ().SetGroupName ("MyFirst").AddConstructor<MyApp> ();
	return tid;
}

void
MyApp::Setup (Ptr<Socket> socket, Address address, uint32_t packetSize, uint32_t nPackets, DataRate dataRate)
{
	m_socket = socket;
	m_peer = address;
	m_packetSize = packetSize;
	m_nPackets = nPackets;
	m_dataRate = dataRate;
}

void
MyApp::StartApplication (void)
{
	m_running = true;
	m_packetsSent = 0;
	if (InetSocketAddress::IsMatchingType (m_peer))
	{
		m_socket->Bind ();
	}
	else
	{
		m_socket->Bind6 ();
	}
	m_socket->Connect (m_peer);
	SendPacket ();
}

void
MyApp::StopApplication (void)
{
	m_running = false;

	if (m_sendEvent.IsPending ())
	{
		Simulator::Cancel (m_sendEvent);
	}

	if (m_socket)
	{
		m_socket->Close ();
	}
}

void
MyApp::SendPacket (void)
{
	Ptr<Packet> packet = Create<Packet> (m_packetSize);
	m_socket->Send (packet);

	if (++m_packetsSent < m_nPackets || m_nPackets == 0)
	{
		ScheduleTx ();
	}
}

void
MyApp::ScheduleTx (void)
{
	if (m_running)
	{
		Time tNext (Seconds (m_packetSize * 8 / static_cast<double> (m_dataRate.GetBitRate ())));
		m_sendEvent = Simulator::Schedule (tNext, &MyApp::SendPacket, this);
	}
}

template <typename T, typename Container = deque<T>>
		class SlidingWindow : public queue<T, Container>
{
private:
	double maxLength;
	double weightedAverage;
public:
	SlidingWindow(int capacity)
	{
		maxLength = capacity;
		weightedAverage = 0.0;
	}
	void push(const T& value)
	{
		if (this->size() == maxLength)
		{
			weightedAverage = weightedAverage - this->front();
			this->pop();
		}
		if(this ->empty()){
			weightedAverage = value;
		}
		else{
			weightedAverage = (value*0.15) + (weightedAverage*0.85);
		}
		queue<T, Container>::push(value);
	}
	double GetWeightedAverage()
	{
		return weightedAverage;
	}
};

static void CwndChange (uint32_t index, uint32_t oldValue, uint32_t newValue)
{
	cwnd[index] = newValue;
}

/*static void PingRtt (uint32_t index, Time oldval, Time newval)
{
	rtt[index] = newval.GetMilliSeconds ();
}*/

static void TraceAQMQueue (uint32_t index, uint32_t oldValue, uint32_t newValue)
{
	softwareQueueValue[index] = newValue;
}

/*static void TraceNetDeviceQueue (uint32_t index, uint32_t oldValue, uint32_t newValue)
{
	hardwareQueueValue[index] = newValue;
}*/

static void TraceRouterBufferLosses (uint32_t index, Ptr< const QueueDiscItem > p)
{
	droppedBytesInBuffer[index] += p ->GetSize();
}

static void TracePacketsInLink (Ptr<const Packet> packet)
{
	bytesInLink += packet->GetSize();
}

static void Configure(uint32_t numberOfNodes, DataRate linkCapacity)
{
	bottleneckLinkCapacity = linkCapacity;
	for(uint32_t count=0; count<numberOfNodes; count++)
	{
		cwnd[count] = 0;
		//rtt[count] = 0;
		previousTxBytes[count] = 0;
		previousDelaySum[count] = 0;
		previousAckCount[count] = 0;
	}
}

static void VariableDatarate(Ptr<PointToPointNetDevice> p2pNetDevice, DataRate linkDataRate, SlidingWindow<double> movingAverage, float timeDelay)
{
	Ptr<UniformRandomVariable> randomVariable = CreateObject<UniformRandomVariable> ();
	randomVariable->SetAttribute ("Max", DoubleValue (linkDataRate.GetBitRate()));
	randomVariable->SetAttribute ("Min", DoubleValue (0));

	movingAverage.push(randomVariable ->GetValue());
	bottleneckLinkCapacity = DataRate(movingAverage.GetWeightedAverage());
	p2pNetDevice ->SetDataRate(bottleneckLinkCapacity);
	Simulator::Schedule (Seconds (timeDelay), &VariableDatarate, p2pNetDevice, linkDataRate, movingAverage, timeDelay);
}

static void RecordNetworkData(float timeDelay)
{
	uint32_t inFlowSoftwareQueueCount = 0/*, inFlowHardwareQueueCount = 0*/;
	for(map <uint32_t, uint32_t>::iterator eachSoftwareQueue = softwareQueueValue.begin(); eachSoftwareQueue != softwareQueueValue.end(); eachSoftwareQueue++)
	{
		inFlowSoftwareQueueCount += eachSoftwareQueue ->second;
	}
	*streamSoftwareQueue ->GetStream () << Simulator::Now ().GetSeconds () << "," << inFlowSoftwareQueueCount << endl;

	/*
	for(map<uint32_t, uint32_t>::iterator eachHardwareQueue = hardwareQueueValue.begin(); eachHardwareQueue != hardwareQueueValue.end(); eachHardwareQueue++)
	{
		inFlowHardwareQueueCount += eachHardwareQueue ->second;
	}

	 *streamHardwareQueue ->GetStream () << Simulator::Now ().GetSeconds () << "," << inFlowHardwareQueueCount << endl;
	 */

	uint32_t droppedBytes = 0;
	for(map<uint32_t, uint32_t>::iterator eachHardwareQueue = droppedBytesInBuffer.begin(); eachHardwareQueue != droppedBytesInBuffer.end(); eachHardwareQueue++)
	{
		droppedBytes += eachHardwareQueue ->second;
		droppedBytesInBuffer[eachHardwareQueue ->first] = 0;
	}
	*streamLinkDrops ->GetStream() << Simulator::Now ().GetSeconds () << "," << droppedBytes << endl;

	/*
	if(firstTraceRouterBufferLosses){
		droppedBytesInBuffer = 0;
		firstTraceRouterBufferLosses = false;
	}
	 *streamLinkDrops ->GetStream() << Simulator::Now ().GetSeconds () << "\t" << droppedBytesInBuffer << endl;
	droppedBytesInBuffer = 0;
	 */

	if(firstTracePacketsInLink){
		bytesInLink = 0;
		firstTracePacketsInLink = false;
	}
	*streamLinkUtilization ->GetStream() << Simulator::Now ().GetSeconds () << "," << (bytesInLink*(8.0)*100)/(bottleneckLinkCapacity.GetBitRate()*timeDelay) << endl;
	*streamLinkCapacity ->GetStream() << Simulator::Now ().GetSeconds () << "," << bottleneckLinkCapacity.GetBitRate()/1000 << endl;// Capacity is measured in Kbps
	bytesInLink = 0;

	Simulator::Schedule (Seconds (timeDelay), &RecordNetworkData, timeDelay);
}

static void RecordUserData(Ptr<FlowMonitor> monitor, vector<uint32_t> senderIndices, float timeDelay)
{
	*streamWindow ->GetStream() << Simulator::Now ().GetSeconds () << "," ;
	//*streamFairness ->GetStream() << Simulator::Now ().GetSeconds () << "," ;
	for(map <uint32_t, uint32_t>::iterator eachWindow = cwnd.begin()/*,eachRTT = rtt.begin()*/; eachWindow!=cwnd.end()/*,eachRTT != rtt.end()*/; eachWindow++/*,eachRTT++*/)
	{
		*streamWindow ->GetStream() << eachWindow ->second << "," ;
		//*streamRTT ->GetStream() << eachRTT ->second << ",";

		/*		if(eachRTT ->second != 0){
		 *streamFairness ->GetStream() << eachWindow ->second / eachRTT ->second /2 << ",";
		}else{
		 *streamFairness ->GetStream() << 0 << ",";
		}
		 */
	}
	*streamWindow ->GetStream() << endl;
	//*streamFairness ->GetStream() << endl;

	map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();
	Time currentTime = Now ();

	*streamTxData ->GetStream() << Simulator::Now ().GetSeconds () << "," ;
	*streamAvgThroughput ->GetStream() << Simulator::Now ().GetSeconds () << "," ;
	*streamThroughput ->GetStream() << Simulator::Now ().GetSeconds () << "," ;
	*streamRTT ->GetStream() << Simulator::Now ().GetSeconds () << "," ;
	for (map<FlowId, FlowMonitor::FlowStats>::iterator eachSenderFlow = stats.begin(); eachSenderFlow != stats.end(); eachSenderFlow++)
	{
		if(std::find(senderIndices.begin(), senderIndices.end(), eachSenderFlow->first) != senderIndices.end())
		{
			*streamTxData ->GetStream() << stats[eachSenderFlow->first].txBytes - previousTxBytes[eachSenderFlow->first] << ",";
			*streamAvgThroughput ->GetStream() << stats[eachSenderFlow->first].txBytes * 8.0 / (currentTime.GetSeconds() * 1000.0) << ",";		// Throughput is measured in Kbps
			*streamThroughput ->GetStream() << (stats[eachSenderFlow->first].txBytes - previousTxBytes[eachSenderFlow->first])* 8.0 / (currentTime.GetSeconds() - previousTime.GetSeconds()) / 1000.0  << ",";

			if(previousAckCount[eachSenderFlow->first] < stats[eachSenderFlow->first].rxPackets){
				*streamRTT ->GetStream() << (stats[eachSenderFlow->first].delaySum.GetMilliSeconds() - previousDelaySum[eachSenderFlow->first])/
						(stats[eachSenderFlow->first].rxPackets - previousAckCount[eachSenderFlow->first]) << ",";
			}else{
				*streamRTT ->GetStream() << 0 << ",";
			}

			previousTxBytes[eachSenderFlow->first] = stats[eachSenderFlow->first].txBytes;
			previousDelaySum[eachSenderFlow->first] = stats[eachSenderFlow->first].delaySum.GetMilliSeconds();
			previousAckCount[eachSenderFlow->first] = stats[eachSenderFlow->first].rxPackets;
		}
	}
	*streamTxData ->GetStream() << endl;
	*streamAvgThroughput ->GetStream() << endl;
	*streamThroughput ->GetStream() << endl;
	*streamRTT ->GetStream() << endl;
	previousTime = currentTime;

	Simulator::Schedule (Seconds (timeDelay), &RecordUserData, monitor, senderIndices, timeDelay);
}

int main(int argc, char **argv) {

	LogComponentEnable(fileName.c_str(), LOG_LEVEL_INFO);
	LogComponentEnable("TcpMadras", LOG_INFO);

	string transportProtocol = "TcpCubic";
	uint32_t mtu = 1500;
	uint32_t longFlows = 3;
	uint32_t shortFlows = 0;
	uint32_t numberOfPackets = 30;				// For short-lived TCP flows, here we take 30 pkts as the deliverable.

	DataRate bottleneckLinkDataRate ("100Mbps");
	Time bottleneckLinkDelay = MilliSeconds(2);
	DataRate endNodeLinkCapacity ("2Mbps");
	Time endNodeLinkDelay = MilliSeconds(0.0001);

	double runtime = 25;
	float traceSamplingTime = 0.05;
	bool tracing = false;
	bool isSack = false;
	bool isDataRateVariable = false;
	bool ifEnableRandomLosses = false;

	string queueDiscType = "PfifoFast";
	string queueSize = "15p";					//follow C*RTT rule for buffer sizing								// The default QueueDisc is a pfifo_fast with a capacity of 1000 packets (as in Linux).
	//string deviceQueueSize = "100p";			//this is the default value in ns3. Check the value in real world cisco routers.

	CommandLine cmd;
	cmd.AddValue ("transportProtocol", "Transport protocol to use: TcpNewReno, "
			"TcpHybla, TcpHighSpeed, TcpHtcp, TcpVegas, TcpScalable, TcpVeno, "
			"TcpBic, TcpYeah, TcpIllinois, TcpWestwood, TcpWestwoodPlus, TcpLedbat, "
			"TcpLp, TcpDctcp, TcpBbr, TcpCompound", transportProtocol);
	cmd.AddValue("runtime", "How long the applications should send data", runtime);
	cmd.AddValue ("longFlows", "Number of long-lived TCp flows", longFlows);
	cmd.AddValue("shortFlows","Number of short-lived TCP flows", shortFlows);
	cmd.AddValue("bottleneckLinkDataRate","The speed at which the bottleneck link serves the data (in Mbps)", bottleneckLinkDataRate);
	cmd.AddValue("bottleneckLinkDelay","The delay at the bottleneck link (in ms)", bottleneckLinkDelay);
	cmd.AddValue("endNodeLinkCapacity","The maximum data rate allowed at the source and destination links (in ms)", endNodeLinkCapacity);
	cmd.AddValue("endNodeLinkDelay","The delay at the source and destination links (in ms)", endNodeLinkDelay);
	cmd.AddValue ("mtu", "Size of IP packets to send in bytes", mtu);
	cmd.AddValue("numberOfPackets", "For Short flows specify the number of Packets to finish the connection", numberOfPackets);
	cmd.AddValue("traceSamplingTime", "Specify time period for observing the queue build up", traceSamplingTime);
	cmd.AddValue("tracing", "Pcap and Animation tracing", tracing);
	cmd.AddValue("isSack", "Selctive Acknowledgment", isSack);
	cmd.AddValue("isDataRateVariable", "Put true to keep the router link's datarate variable", isDataRateVariable);
	cmd.AddValue("ifEnableRandomLosses", "Enable Random losses on  Bottleneck Link", ifEnableRandomLosses);
	cmd.AddValue ("queueDiscType", "Bottleneck queue disc type in {PfifoFast, ARED, CoDel, FqCoDel, PIE, prio}", queueDiscType);
	cmd.AddValue("queueSize","Mention the queue size for the bottleneck link", queueSize);
	//cmd.AddValue("deviceQueueSize","Mention the hardware queue size for the bottleneck link", deviceQueueSize);
	cmd.Parse (argc, argv);

	transportProtocol = string ("ns3::") + transportProtocol;
	NS_LOG_INFO("TCP variant is:" << TypeId::LookupByName (transportProtocol));

	//when internet stacks are added to the node, the default TCP implementation that is aggregated to the node is the ns-3 TCP. This can be overridden as we show below when using Network Simulation Cradle.
	Config::SetDefault ("ns3::TcpL4Protocol::SocketType", TypeIdValue (TypeId::LookupByName (transportProtocol)));
	Config::SetDefault ("ns3::TcpSocket::SegmentSize", UintegerValue (mtu));
	Config::SetDefault ("ns3::TcpSocketBase::Sack", BooleanValue (isSack));
	Config::SetDefault ("ns3::TcpSocketBase::WindowScaling", BooleanValue (true));						// Enabling window scaling option to increase the window size to 1Gbits in high bandwidth delay networks
	Config::SetDefault ("ns3::TcpSocket::DelAckCount", UintegerValue (1));										// Number of packets to wait before sending an acknowledgment
	Config::SetDefault("ns3::Ipv4L3Protocol::DefaultTtl", UintegerValue (64));							// Time to live = 64 means each router decreases this pkt by 1.
	Config::SetDefault ("ns3::TcpSocket::InitialCwnd", UintegerValue (1));								// Initial Cwnd = 10 pkts
	Config::SetDefault ("ns3::DropTailQueue<Packet>::MaxSize", QueueSizeValue (QueueSize ("1p")));		// Change the Hardware Buffer Capacity, if we choose to have different sizes for different links

	NS_LOG_INFO ("Creating links (network cables) and nodes");
	PointToPointHelper routerLink, routerToSourceLink, routerToSinkLink;								//check the network for Large bandwidth delay product >= 10^5 bits
	routerLink.SetDeviceAttribute("DataRate", DataRateValue(bottleneckLinkDataRate));
	routerLink.SetChannelAttribute("Delay", TimeValue(bottleneckLinkDelay));													// usually this bottleneck link delay is comparable to RTT like 100ms
	//routerLink.SetQueue("ns3::DropTailQueue", "MaxSize", StringValue(deviceQueueSize)); 											//hardware(physical) buffer, default is 100 packets

	routerToSourceLink.SetDeviceAttribute("DataRate", DataRateValue(endNodeLinkCapacity));
	routerToSourceLink.SetChannelAttribute("Delay", TimeValue(endNodeLinkDelay));
	//routerToSourceLink.SetQueue("ns3::DropTailQueue", "MaxSize", StringValue(deviceQueueSize));

	routerToSinkLink.SetDeviceAttribute("DataRate", DataRateValue(endNodeLinkCapacity));
	routerToSinkLink.SetChannelAttribute("Delay", TimeValue(endNodeLinkDelay));
	//routerToSinkLink.SetQueue("ns3::DropTailQueue", "MaxSize", StringValue(deviceQueueSize));

	uint32_t numberOfTCPNodes = longFlows + shortFlows;
	PointToPointDumbbellHelper dumbbell(numberOfTCPNodes, routerToSourceLink, numberOfTCPNodes, routerToSinkLink, routerLink);

	NS_LOG_INFO ("Installing Internet Protocol stacks on all nodes");
	InternetStackHelper internetStackHelper;
	dumbbell.InstallStack(internetStackHelper);

	if(ifEnableRandomLosses){
		NS_LOG_INFO ("Introducing some ambiguity in reception of packets at bottleneck link");
		Ptr<RateErrorModel> errorIntroduced = CreateObject<RateErrorModel> ();
		errorIntroduced->SetAttribute ("ErrorRate", DoubleValue (0.00001));
		(dumbbell.GetRight() ->GetDevice(0))->SetAttribute ("ReceiveErrorModel", PointerValue (errorIntroduced));
	}

	NS_LOG_INFO("Assigning an AQM for the netdevices");
	TrafficControlHelper bottleneckLinkAQM/*, accessLinkAQM*/;
	/*	accessLinkAQM.SetRootQueueDisc("ns3::PfifoFastQueueDisc", "MaxSize", StringValue("1000p"));
	//accessLinkAQM.SetQueueLimits ("ns3::DynamicQueueLimits", "HoldTime", StringValue ("4ms"));							//use for only LINUX host systems
	QueueDiscContainer clientQueueDisc = accessLinkAQM.Install(clientDevices);
	QueueDiscContainer serverQueueDisc = accessLinkAQM.Install(serverDevices);*/

	if (queueDiscType.compare ("PfifoFast") == 0)
	{
		bottleneckLinkAQM.SetRootQueueDisc ("ns3::PfifoFastQueueDisc");				// software(NIC device's) queue, apply this before assigning the IPV4 addresses
		Config::SetDefault ("ns3::PfifoFastQueueDisc::MaxSize",
				QueueSizeValue (QueueSize (queueSize)));
	}
	else if (queueDiscType.compare ("ARED") == 0)
	{
		bottleneckLinkAQM.SetRootQueueDisc ("ns3::RedQueueDisc");
		Config::SetDefault ("ns3::RedQueueDisc::ARED", BooleanValue (true));
		Config::SetDefault ("ns3::RedQueueDisc::MaxSize",
				QueueSizeValue (QueueSize (queueSize)));
		Config::SetDefault ("ns3::RedQueueDisc::UseEcn", BooleanValue (true));
	}
	else if (queueDiscType.compare ("CoDel") == 0)
	{
		bottleneckLinkAQM.SetRootQueueDisc ("ns3::CoDelQueueDisc");
		Config::SetDefault ("ns3::CoDelQueueDisc::MaxSize",
				QueueSizeValue (QueueSize (queueSize)));
	}
	else if (queueDiscType.compare ("FqCoDel") == 0)
	{
		bottleneckLinkAQM.SetRootQueueDisc ("ns3::FqCoDelQueueDisc");
		Config::SetDefault ("ns3::FqCoDelQueueDisc::MaxSize",
				QueueSizeValue (QueueSize (queueSize)));
	}
	else if (queueDiscType.compare ("PIE") == 0)
	{
		bottleneckLinkAQM.SetRootQueueDisc ("ns3::PieQueueDisc");
		Config::SetDefault ("ns3::PieQueueDisc::MaxSize",
				QueueSizeValue (QueueSize (queueSize)));
	}
	else if (queueDiscType.compare ("prio") == 0)
	{
		uint16_t handle = bottleneckLinkAQM.SetRootQueueDisc ("ns3::PrioQueueDisc", "Priomap",
				StringValue ("0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1"));
		TrafficControlHelper::ClassIdList cid = bottleneckLinkAQM.AddQueueDiscClasses (handle, 2, "ns3::QueueDiscClass");
		bottleneckLinkAQM.AddChildQueueDisc (handle, cid[0], "ns3::FifoQueueDisc");
		bottleneckLinkAQM.AddChildQueueDisc (handle, cid[1], "ns3::RedQueueDisc");
	}
	else
	{
		bottleneckLinkAQM.SetRootQueueDisc ("ns3::FifoQueueDisc");
		Config::SetDefault ("ns3::FifoQueueDisc::MaxSize",
				QueueSizeValue (QueueSize (queueSize)));
	}

	vector<QueueDiscContainer> leftRouterQueueDiscs(dumbbell.GetLeft() ->GetNDevices()), rightRouterQueueDiscs(dumbbell.GetRight() ->GetNDevices());
	for (uint32_t eachLeftNodeDevice = 0; eachLeftNodeDevice < dumbbell.GetLeft() ->GetNDevices(); ++eachLeftNodeDevice)
	{
		leftRouterQueueDiscs[eachLeftNodeDevice] = bottleneckLinkAQM.Install(dumbbell.GetLeft() ->GetDevice(eachLeftNodeDevice));
	}
	for (uint32_t eachRightNodeDevice = 0; eachRightNodeDevice < dumbbell.GetRight() ->GetNDevices(); ++eachRightNodeDevice)
	{
		rightRouterQueueDiscs[eachRightNodeDevice] = bottleneckLinkAQM.Install(dumbbell.GetRight() ->GetDevice(eachRightNodeDevice));
	}

	NS_LOG_INFO ("Assign IP Addresses");
	Ipv4AddressHelper sourceIPAddresses, sinkIPAddresses, routerIPAddresses;
	sourceIPAddresses.SetBase ("192.168.10.0", "255.255.255.0");
	sinkIPAddresses.SetBase("172.1.1.0", "255.255.255.0");
	routerIPAddresses.SetBase("10.10.1.0","255.255.255.0");

	dumbbell.AssignIpv4Addresses(sourceIPAddresses, sinkIPAddresses, routerIPAddresses);

	NS_LOG_INFO ("Enable static global routing");
	Ipv4GlobalRoutingHelper::PopulateRoutingTables ();

	NS_LOG_INFO ("Creating an Application");
	uint16_t rightReceivingPortNumber = 8080;

	vector<Ptr<Socket>> sourceSocket(numberOfTCPNodes);
	ApplicationContainer destinationApp;
	vector<uint32_t> sendersIndexList;

	double shape = 1.4;
	Ptr<ParetoRandomVariable> fileDistribution = CreateObject<ParetoRandomVariable> ();
	fileDistribution->SetAttribute ("Scale", DoubleValue (numberOfPackets*(shape-1)/shape));							// E(X) = Shape*Scale / (Shape -1)
	fileDistribution->SetAttribute ("Shape", DoubleValue (shape));														// Mean = 30 pkts, Shape = 1.4. Hence, Scale = 8.5714

	shape = 1.0;
	Ptr<WeibullRandomVariable> flowArrivalProcess = CreateObject<WeibullRandomVariable> ();
	flowArrivalProcess->SetAttribute ("Scale", DoubleValue (runtime/10));		// E[value]  =  scale * Gamma(1 + 1 / shape); here mean is half of runtime
	flowArrivalProcess->SetAttribute ("Shape", DoubleValue (shape));

	/*Include Skype or Videocalling application in our NS3 simulator
	 * and compare performance against file uploads*/
	for(uint32_t i = 0; i < numberOfTCPNodes; ++i)
	{
		Address sinkLocalAddress(InetSocketAddress (dumbbell.GetRightIpv4Address(i), rightReceivingPortNumber));
		sourceSocket[i] = Socket::CreateSocket(dumbbell.GetLeft(i), TcpSocketFactory::GetTypeId());
		sourceSocket[i] -> TraceConnectWithoutContext ("CongestionWindow", MakeBoundCallback (&CwndChange, i));
		//sourceSocket[i] -> TraceConnectWithoutContext("RTT", MakeBoundCallback (&PingRtt, i));
		sendersIndexList.push_back((sourceSocket[i] -> GetNode()) ->GetId());

		Ptr<MyApp> myAppObject = CreateObject<MyApp> ();
		if (i<longFlows){
			myAppObject -> Setup(sourceSocket[i], sinkLocalAddress, mtu, 0, DataRate (10*endNodeLinkCapacity.GetBitRate()));		// Here application prepares 10 times the access link's bandwidth to the TCP socket's buffer.
			dumbbell.GetLeft(i) -> AddApplication(myAppObject);
			myAppObject -> SetStartTime(Seconds (flowArrivalProcess->GetValue ()));
			myAppObject -> SetStopTime(Seconds (runtime));
		}
		else{
			myAppObject -> Setup(sourceSocket[i], sinkLocalAddress, mtu, fileDistribution -> GetValue(), DataRate (10*endNodeLinkCapacity.GetBitRate()));
			dumbbell.GetLeft(i) -> AddApplication(myAppObject);
			myAppObject -> SetStartTime(Seconds (flowArrivalProcess->GetValue ()));
			myAppObject -> SetStopTime(Seconds (runtime));
		}

		PacketSinkHelper sinkHelper ("ns3::TcpSocketFactory", sinkLocalAddress);
		sinkHelper.SetAttribute ("Protocol", TypeIdValue (TcpSocketFactory::GetTypeId ()));
		destinationApp.Add(sinkHelper.Install(dumbbell.GetRight(i)));
		destinationApp.Start(Seconds(0.01));
		destinationApp.Stop(Seconds(runtime));
	}

	dumbbell.BoundingBox (1, 1, 100, 100);
	AnimationInterface anim (fileName+"Animation.xml");
	anim.EnablePacketMetadata (); // Optional
	anim.EnableIpv4L3ProtocolCounters (Seconds (0), Seconds (5)); // Optional
	NS_LOG_INFO("Animation Trace file created");

	if(tracing)										// For WIRESHARK and to plot I/O statistics graphs
	{
		NS_LOG_INFO ("Wireshark Trace file (.pcap) is generated");
		routerToSourceLink.EnablePcapAll(fileName+"SourcePcap", false);
		routerToSinkLink.EnablePcapAll(fileName+"SinkPcap", false);
	}

	AsciiTraceHelper asciiTraceHelper;
	streamWindow = asciiTraceHelper.CreateFileStream(fileName+"Window.csv");
	streamRTT = asciiTraceHelper.CreateFileStream(fileName+"RTT.csv");
	streamSoftwareQueue = asciiTraceHelper.CreateFileStream(fileName+"SoftwareQueue.csv");
	streamLinkDrops = asciiTraceHelper.CreateFileStream(fileName+"Drops.csv");
	streamLinkUtilization = asciiTraceHelper.CreateFileStream(fileName+"Utilization.csv");
	streamFlowMonitor = asciiTraceHelper.CreateFileStream(fileName+"FlowMonitor.dat");
	streamTxData = asciiTraceHelper.CreateFileStream(fileName+"TxData.csv");
	streamAvgThroughput = asciiTraceHelper.CreateFileStream(fileName+"AvgThroughput.csv");
	streamThroughput = asciiTraceHelper.CreateFileStream(fileName+"Throughput.csv");
	//streamFairness = asciiTraceHelper.CreateFileStream(fileName+"FairEstimate.csv");
	streamLinkCapacity = asciiTraceHelper.CreateFileStream(fileName+"LinkCapacity.csv");

	/*	for (uint32_t eachLeftNodeDevice = 0; eachLeftNodeDevice < dumbbell.GetLeft() ->GetNDevices(); ++eachLeftNodeDevice)
	{
		Ptr<QueueDisc> queueDisc = leftRouterQueueDiscs[eachLeftNodeDevice].Get(0);
		queueDisc->TraceConnectWithoutContext ("PacketsInQueue", MakeBoundCallback (&TraceAQMQueue, eachLeftNodeDevice)); //check without iterator
	}

	for (uint32_t eachLeftNodeNetDevice = 0; eachLeftNodeNetDevice < dumbbell.GetLeft() ->GetNDevices() - 1; ++eachLeftNodeNetDevice)	//except the first Device with is an output device
	{
		Ptr<NetDevice> nd = dumbbell.GetLeft() ->GetDevice(eachLeftNodeNetDevice);
		Ptr<PointToPointNetDevice> ptpnd = StaticCast<PointToPointNetDevice> (nd);
		Ptr<Queue<Packet> > hardwareQueue = ptpnd->GetQueue ();
		hardwareQueue ->TraceConnectWithoutContext ("PacketsInQueue", MakeBoundCallback (&TraceNetDeviceQueue, eachLeftNodeNetDevice));
		hardwareQueue ->TraceConnectWithoutContext("Drop", MakeBoundCallback(&TraceRouterBufferLosses, eachLeftNodeNetDevice));
	}*/
	Ptr<QueueDisc> queueDisc = leftRouterQueueDiscs[0].Get(0);
	queueDisc ->TraceConnectWithoutContext ("PacketsInQueue", MakeBoundCallback(&TraceAQMQueue, 0));
	queueDisc ->TraceConnectWithoutContext("Drop", MakeBoundCallback(&TraceRouterBufferLosses, 0));

	Ptr<NetDevice> leftRouterNetDevice = dumbbell.GetLeft() ->GetDevice(0);
	Ptr<PointToPointNetDevice> leftP2pNetDevice = StaticCast<PointToPointNetDevice> (leftRouterNetDevice);
	Ptr<Queue<Packet> > hardwareQueue = leftP2pNetDevice->GetQueue ();
	//hardwareQueue -> TraceConnectWithoutContext ("PacketsInQueue", MakeBoundCallback(&TraceNetDeviceQueue, 0));
	hardwareQueue -> TraceConnectWithoutContext("Dequeue", MakeCallback (&TracePacketsInLink));

	if(isDataRateVariable)
	{
		Time slidingWindowTime = Seconds(2.0);
		SlidingWindow<double> movingAverage(static_cast<int>(slidingWindowTime.GetDouble()/traceSamplingTime));
		Simulator::Schedule (Seconds (0.5), &VariableDatarate, leftP2pNetDevice, bottleneckLinkDataRate, movingAverage, traceSamplingTime);
	}

	NS_LOG_INFO("Monitoring each flow");
	FlowMonitorHelper flowmon;
	Ptr<FlowMonitor> monitor = flowmon.InstallAll();

	Simulator::Schedule(Seconds (0.0), &Configure, numberOfTCPNodes, bottleneckLinkDataRate);
	Simulator::Schedule(Seconds (1.0), &RecordNetworkData, traceSamplingTime);
	Simulator::Schedule(Seconds (1.0), &RecordUserData, monitor, sendersIndexList, traceSamplingTime);

	Simulator::Stop (Seconds (runtime));
	NS_LOG_INFO ("Running Simulation");
	Simulator::Run ();

	monitor->SerializeToXmlFile(fileName+"FlowMonitor.xml", false, false);

	map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();				//a flow is defined as the packets with the same {protocol, source (IP, port), destination (IP, port)} tuple.
	*streamFlowMonitor ->GetStream() << endl << "*** Flow monitor statistics ***" << endl;
	for(map<uint32_t, FlowMonitor::FlowStats>::iterator flowId = stats.begin (); flowId != stats.end (); flowId++){
		*streamFlowMonitor ->GetStream() << "------------ \t" << flowId->first << "\t ----------------" << endl;
		*streamFlowMonitor ->GetStream() << "  Tx Packets/Bytes:   " << stats[flowId->first].txPackets << " / " << stats[flowId->first].txBytes << endl;
		*streamFlowMonitor ->GetStream() << "  Offered Load: " << stats[flowId->first].txBytes * 8.0 / (stats[flowId->first].timeLastTxPacket.GetSeconds () - stats[flowId->first].timeFirstTxPacket.GetSeconds ()) / 1000000 << " Mbps" << endl;
		*streamFlowMonitor ->GetStream() << "  Rx Packets/Bytes:   " << stats[flowId->first].rxPackets << " / " << stats[flowId->first].rxBytes << endl;

		uint32_t packetsDroppedByQueueDisc = 0;
		uint64_t bytesDroppedByQueueDisc = 0;
		if (stats[flowId->first].packetsDropped.size () > Ipv4FlowProbe::DROP_QUEUE_DISC)
		{
			packetsDroppedByQueueDisc = stats[flowId->first].packetsDropped[Ipv4FlowProbe::DROP_QUEUE_DISC];
			bytesDroppedByQueueDisc = stats[flowId->first].bytesDropped[Ipv4FlowProbe::DROP_QUEUE_DISC];
		}
		*streamFlowMonitor ->GetStream() << "  Packets/Bytes Dropped by Queue Disc:   " << packetsDroppedByQueueDisc << " / " << bytesDroppedByQueueDisc << endl;

		uint32_t packetsDroppedByNetDevice = 0;
		uint64_t bytesDroppedByNetDevice = 0;
		if (stats[flowId->first].packetsDropped.size () > Ipv4FlowProbe::DROP_QUEUE)
		{
			packetsDroppedByNetDevice = stats[flowId->first].packetsDropped[Ipv4FlowProbe::DROP_QUEUE];
			bytesDroppedByNetDevice = stats[flowId->first].bytesDropped[Ipv4FlowProbe::DROP_QUEUE];
		}
		*streamFlowMonitor ->GetStream() << "  Packets/Bytes Dropped by NetDevice:   " << packetsDroppedByNetDevice << " / " << bytesDroppedByNetDevice << endl;

		*streamFlowMonitor ->GetStream() << "  Throughput: " << stats[flowId->first].rxBytes * 8.0 / (stats[flowId->first].timeLastRxPacket.GetSeconds () - stats[flowId->first].timeFirstRxPacket.GetSeconds ()) / 1000000 << " Mbps" << endl;
		*streamFlowMonitor ->GetStream() << "  Mean delay:   " << stats[flowId->first].delaySum.GetSeconds () / stats[flowId->first].rxPackets << endl;
		*streamFlowMonitor ->GetStream() << "  Mean jitter:   " << stats[flowId->first].jitterSum.GetSeconds () / (stats[flowId->first].rxPackets - 1) << endl;
		*streamFlowMonitor ->GetStream() << "\n" << endl;
	}

	Simulator::Destroy ();
	/*	NS_LOG_INFO ( "*** Application statistics for 1st node***" );
	double thr = 0;
	uint64_t totalBytesReceived = DynamicCast<PacketSink> (destinationApp.Get (0))->GetTotalRx ();
	thr = totalBytesReceived * 8 / (runtime * 1000000.0); //Mbit/s
	NS_LOG_INFO ( "  Rx Bytes: " << totalBytesReceived );
	NS_LOG_INFO ( "  Average Goodput: " << thr << " Mbit/s" );
	*/
	/*	NS_LOG_INFO("*** TC Layer statistics from left node of the bottleneck link ***");
	QueueDisc::Stats statistics = leftRouterQueueDiscs[0].Get(0) -> GetStats();
	NS_LOG_INFO(statistics);
*/
return 0;
}
