/*
 * cluster.cc
 *
 *  Created on: 30-Jun-2019
 *
 *  Last Modified on: 16-July-2021
 *      Author: Himateja Nallani <himateja.nallani@gmail.com>
 */

/*
#include <iomanip>
#include <iostream>
#include <fstream>
#include <string>
#include <iterator> // for iterators
#include <random>
#include <map>
#include <utility>
#include <queue>
#include <deque>
*/
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/applications-module.h"
#include "ns3/traffic-control-module.h"
#include "ns3/flow-monitor-module.h"

using namespace ns3;
using namespace std;

static string fileName = "clusters";
static uint32_t bytesInLink = 0;
Time previousTime = Seconds (0);
static map<uint32_t, uint32_t> cwnd, rtt, previousDelaySum, txBytes, previousTxBytes, rxAckCount, previousAckCount, softwareQueueValue, droppedBytesInBuffer;
static bool firstTracePacketsInLink = true;
//static vector<bool> firstTraceRouterBufferLosses =true;
static Ptr<OutputStreamWrapper> streamSoftwareQueue, streamLinkUtilization, streamLinkDrops,
	streamWindow, /*streamOneWayDelay,*/ streamFlowMonitor, streamTxData, streamAvgThroughput,
	streamThroughput, streamFairness, streamLinkCapacity, streamRTT;
static DataRate bottleneckLinkCapacity;

NS_LOG_COMPONENT_DEFINE(fileName);

class MyApp : public Application
{
public:
	MyApp ();
	virtual ~MyApp ();

	/**
	 * Register this type.
	 * \return The TypeId.
	 */
	static TypeId GetTypeId (void);
	void Setup (Ptr<Socket> socket, Address address, uint32_t packetSize, uint32_t nPackets, DataRate dataRate);

private:
	virtual void StartApplication (void);
	virtual void StopApplication (void);

	void ScheduleTx (void);
	void SendPacket (void);

	Ptr<Socket>     m_socket;
	Address         m_peer;
	uint32_t        m_packetSize;
	uint32_t        m_nPackets;
	DataRate        m_dataRate;
	EventId         m_sendEvent;
	bool            m_running;
	uint32_t        m_packetsSent;
};

MyApp::MyApp ()
: m_socket (0),
  m_peer (),
  m_packetSize (0),
  m_nPackets (0),
  m_dataRate (0),
  m_sendEvent (),
  m_running (false),
  m_packetsSent (0)
{
}

MyApp::~MyApp ()
{
	m_socket = 0;
}

TypeId MyApp::GetTypeId (void)
{
	static TypeId tid = TypeId ("MyApp").SetParent<Application> ().SetGroupName ("MyFirst").AddConstructor<MyApp> ();
	return tid;
}

void
MyApp::Setup (Ptr<Socket> socket, Address address, uint32_t packetSize, uint32_t nPackets, DataRate dataRate)
{
	m_socket = socket;
	m_peer = address;
	m_packetSize = packetSize;
	m_nPackets = nPackets;
	m_dataRate = dataRate;
}

void
MyApp::StartApplication (void)
{
	m_running = true;
	m_packetsSent = 0;
	if (InetSocketAddress::IsMatchingType (m_peer))
	{
		m_socket->Bind ();
	}
	else
	{
		m_socket->Bind6 ();
	}
	m_socket->Connect (m_peer);
	SendPacket ();
}

void
MyApp::StopApplication (void)
{
	m_running = false;

	if (m_sendEvent.IsPending ())
	{
		Simulator::Cancel (m_sendEvent);
	}

	if (m_socket)
	{
		m_socket->Close ();
	}
}

void
MyApp::SendPacket (void)
{
	Ptr<Packet> packet = Create<Packet> (m_packetSize);
	m_socket->Send (packet);

	if (++m_packetsSent < m_nPackets || m_nPackets == 0)
	{
		ScheduleTx ();
	}
}

void
MyApp::ScheduleTx (void)
{
	if (m_running)
	{
		Time tNext (Seconds (m_packetSize * 8 / static_cast<double> (m_dataRate.GetBitRate ())));
		m_sendEvent = Simulator::Schedule (tNext, &MyApp::SendPacket, this);
	}
}

template <typename T, typename Container = deque<T>>
		class SlidingWindow : public queue<T, Container>
{
private:
	double maxLength;
	double weightedAverage;
public:
	SlidingWindow(int capacity)
	{
		maxLength = capacity;
		weightedAverage = 0.0;
	}
	void push(const T& value)
	{
		if (this->size() == maxLength)
		{
			weightedAverage = weightedAverage - this->front();
			this->pop();
		}
		if(this ->empty()){
			weightedAverage = value;
		}
		else{
			weightedAverage = (value*0.15) + (weightedAverage*0.85);
		}
		queue<T, Container>::push(value);
	}
	double GetWeightedAverage()
	{
		return weightedAverage;
	}
};

static void CwndChange (uint32_t index, uint32_t oldValue, uint32_t newValue)
{
	cwnd[index] = newValue;
}

static void PingRtt (uint32_t index, Time oldval, Time newval)
{
	rtt[index] += newval.GetMilliSeconds ();
}

static void TraceTx (uint32_t index, Ptr<const Packet> packet, const TcpHeader& header, Ptr<const TcpSocketBase> socket)
{
	txBytes[index] += packet->GetSize();
}

static void TraceRx (uint32_t index, Ptr<const Packet> packet, const TcpHeader& header, Ptr<const TcpSocketBase> socket)
{
	rxAckCount[index] += 1;
}

static void TraceAQMQueue (uint32_t index, uint32_t oldValue, uint32_t newValue)
{
	softwareQueueValue[index] = newValue;
}

static void TraceRouterBufferLosses (uint32_t index, Ptr< const QueueDiscItem > p)
{
	droppedBytesInBuffer[index] += p ->GetSize();
}

static void TracePacketsInLink (Ptr<const Packet> packet)
{
	bytesInLink += packet->GetSize();
}

static void Configure(vector<uint32_t> indices, DataRate linkCapacity)
{
	bottleneckLinkCapacity = linkCapacity;
	for(auto index: indices)
	{
		cwnd[index] = 0;
		rtt[index] = 0;
		//txBytes[index] = 0; //Don't initialize this @t=0, will count Jain index for flows yet to have started.
		rxAckCount[index] = 0;
		previousTxBytes[index] = 0;
		previousDelaySum[index] = 0;
		previousAckCount[index] = 0;
	}
}

static void VariableDatarate(Ptr<PointToPointNetDevice> p2pNetDevice, DataRate linkDataRate, SlidingWindow<double> movingAverage, float timeDelay)
{
	Ptr<UniformRandomVariable> randomVariable = CreateObject<UniformRandomVariable> ();
	randomVariable -> SetAttribute ("Max", DoubleValue (linkDataRate.GetBitRate()));
	randomVariable -> SetAttribute ("Min", DoubleValue (0));

	movingAverage.push(randomVariable ->GetValue());
	bottleneckLinkCapacity = DataRate(movingAverage.GetWeightedAverage());
	p2pNetDevice -> SetDataRate(bottleneckLinkCapacity);
	Simulator::Schedule (Seconds (timeDelay), &VariableDatarate, p2pNetDevice, linkDataRate, movingAverage, timeDelay);
}

static void VariableDelay(Ptr<PointToPointNetDevice> p2pNetDevice, float timeDelay)
{
	Ptr<UniformRandomVariable> randomVariable = CreateObject<UniformRandomVariable> ();
	randomVariable -> SetAttribute ("Max", DoubleValue (100));		// Max 100ms
	randomVariable -> SetAttribute ("Min", DoubleValue (1));  		// Min 1ms

	Ptr<PointToPointChannel> p2pChannel = DynamicCast<PointToPointChannel>(p2pNetDevice -> GetChannel());
	p2pChannel -> SetAttribute("Delay", TimeValue(MilliSeconds(randomVariable ->GetValue())));

	Simulator::Schedule (Seconds (timeDelay), &VariableDelay, p2pNetDevice, timeDelay);
}

static void VariableRandomLoss(Ptr<PointToPointNetDevice> p2pNetDevice, float timeDelay)
{
	Ptr<UniformRandomVariable> randomVariable = CreateObject<UniformRandomVariable> ();
	randomVariable -> SetAttribute ("Max", DoubleValue (1));		// Max 1% random pkt drops
	randomVariable -> SetAttribute ("Min", DoubleValue (0));

	Ptr<RateErrorModel> errorIntroduced = CreateObject<RateErrorModel> ();
	errorIntroduced -> SetAttribute ("ErrorRate", DoubleValue (randomVariable -> GetValue()));
	p2pNetDevice -> SetReceiveErrorModel(errorIntroduced);
	Simulator::Schedule (Seconds (timeDelay), &VariableRandomLoss, p2pNetDevice, timeDelay);
}

static void RecordNetworkData(float timeDelay)
{
	uint32_t inFlowSoftwareQueueCount = 0/*, inFlowHardwareQueueCount = 0*/;
	for(map <uint32_t, uint32_t>::iterator eachSoftwareQueue = softwareQueueValue.begin(); eachSoftwareQueue != softwareQueueValue.end(); eachSoftwareQueue++)
	{
		inFlowSoftwareQueueCount += eachSoftwareQueue ->second;
	}
	*streamSoftwareQueue ->GetStream () << Simulator::Now ().GetSeconds () << "," << inFlowSoftwareQueueCount << endl;

	uint32_t droppedBytes = 0;
	for(map<uint32_t, uint32_t>::iterator eachHardwareQueue = droppedBytesInBuffer.begin(); eachHardwareQueue != droppedBytesInBuffer.end(); eachHardwareQueue++)
	{
		droppedBytes += eachHardwareQueue ->second;
		droppedBytesInBuffer[eachHardwareQueue ->first] = 0;
	}
	*streamLinkDrops ->GetStream() << Simulator::Now ().GetSeconds () << "," << droppedBytes << endl;

	if(firstTracePacketsInLink){
		bytesInLink = 0;
		firstTracePacketsInLink = false;
	}
	*streamLinkUtilization ->GetStream() << Simulator::Now ().GetSeconds () << "," << (bytesInLink * 8.0 * 100) / (bottleneckLinkCapacity.GetBitRate() * timeDelay) << endl;
	*streamLinkCapacity ->GetStream() << Simulator::Now ().GetSeconds () << "," << bottleneckLinkCapacity.GetBitRate() / pow(2,20) << endl;// Capacity is measured in Mbps
	bytesInLink = 0;

	Simulator::Schedule (Seconds (timeDelay), &RecordNetworkData, timeDelay);
}

static void RecordUserData(float timeDelay)
{
	*streamWindow ->GetStream() << Simulator::Now ().GetSeconds () << "," ;
	for(map <uint32_t, uint32_t>::iterator eachWindow = cwnd.begin(); eachWindow != cwnd.end(); eachWindow++)
	{
		*streamWindow ->GetStream() << eachWindow ->second << "," ;
	}
	*streamWindow ->GetStream() << endl;

	*streamRTT ->GetStream() << Simulator::Now ().GetSeconds () << "," ;
	for(pair<map <uint32_t, uint32_t>::iterator, map <uint32_t, uint32_t>::iterator> eachSource (rtt.begin(), rxAckCount.begin());
			eachSource.first != rtt.end(); ++(eachSource.first), ++(eachSource.second))
	{
		if(previousAckCount[(eachSource.second)->first] < rxAckCount[(eachSource.second)->first]){
			*streamRTT ->GetStream() << (rtt[(eachSource.first)->first] - previousDelaySum[(eachSource.first)->first])/
					(rxAckCount[(eachSource.second)->first] - previousAckCount[(eachSource.second)->first]) << ",";
		}else{
			*streamRTT ->GetStream() << " " << ",";
		}

		previousDelaySum[(eachSource.first)->first] = rtt[(eachSource.first)->first]; 	//Each RTT is gathered in Milliseconds
		previousAckCount[(eachSource.second)->first] = rxAckCount[(eachSource.second)->first];
	}
	*streamRTT ->GetStream() << endl;

	Time currentTime = Now ();

	*streamTxData ->GetStream() << Simulator::Now ().GetSeconds () << "," ;
	*streamAvgThroughput ->GetStream() << Simulator::Now ().GetSeconds () << "," ;
	*streamThroughput ->GetStream() << Simulator::Now ().GetSeconds () << "," ;
	//*streamOneWayDelay ->GetStream() << Simulator::Now ().GetSeconds () << "," ;
	*streamFairness ->GetStream() << std::setprecision(6) << Simulator::Now ().GetSeconds () << "," ;

	double sumOfThroughputs = 0, sumOfThroughputSquares = 0;
	for(map <uint32_t, uint32_t>::iterator eachSource = txBytes.begin(); eachSource != txBytes.end(); eachSource++)
	{
		*streamTxData ->GetStream() << (eachSource ->second) - previousTxBytes[eachSource->first] << ",";
		*streamAvgThroughput ->GetStream() << (eachSource ->second) * 8.0 / (currentTime.GetSeconds() * pow(2,20)) << ",";
		*streamThroughput ->GetStream() << (eachSource ->second - previousTxBytes[eachSource->first])* 8.0 / (currentTime.GetSeconds() - previousTime.GetSeconds()) / pow(2,20) << ",";
		sumOfThroughputs += (eachSource ->second) * 8.0 / (currentTime.GetSeconds() * pow(2,20));
		sumOfThroughputSquares += pow((eachSource ->second) * 8.0 / (currentTime.GetSeconds() * pow(2,20)), 2);

		previousTxBytes[eachSource->first] = (eachSource ->second);
	}
	*streamFairness ->GetStream() << std::setprecision(4) << ((sumOfThroughputs > 0) ? ((pow(sumOfThroughputs, 2)) / (txBytes.size() * sumOfThroughputSquares)) : 0) << "," ;

	*streamTxData ->GetStream() << endl;
	*streamAvgThroughput ->GetStream() << endl;
	*streamThroughput ->GetStream() << endl;
	//*streamOneWayDelay ->GetStream() << endl;
	*streamFairness ->GetStream() << endl;
	previousTime = currentTime;

	Simulator::Schedule (Seconds (timeDelay), &RecordUserData, timeDelay);
}

static Ipv4Address GetAddressFromNode (Ptr<Node> node){
	Ptr<Ipv4> ipv4 = node -> GetObject<Ipv4>();
	int32_t interfaceLocal = ipv4 -> GetInterfaceForDevice(node -> GetDevice(0));
	Ipv4Address address = ipv4 -> GetAddress (interfaceLocal, 0).GetLocal ();
	return address;
}

int main(int argc, char **argv) {

	LogComponentEnable(fileName.c_str(), LOG_LEVEL_INFO);
	LogComponentEnable("TcpMadras", LOG_INFO);

	list<string> transportProtocol /*= {"TcpCubic"}*/;
	uint32_t mtu = 1500;
	uint32_t longFlows = 3;
	uint32_t shortFlows = 0;
	uint32_t numberOfPackets = 30;				// For short-lived TCP flows, here we take 30 pkts as the deliverable.

	DataRate bottleneckLinkDataRate ("100Mbps");
	Time bottleneckLinkDelay = MilliSeconds(2);
	DataRate endNodeLinkCapacity ("2Mbps");
	set<Time> endNodeLinkDelay /*= {MilliSeconds(10), MilliSeconds(100)}*/;
	string tcps, delays;

	double runtime = 25;
	float traceSamplingTime = 0.05;
	bool tracing = false;
	bool isSack = false;
	bool isDataRateVariable = false;
	bool isRandomLossVariable = false;
	bool isDelayVariable = false;
	bool isRandomArrival = false;

	string queueDiscType = "FifoQueueDisc";
	string queueSize = "15p";					//follow C*RTT rule for buffer sizing	// The default QueueDisc is a pfifo_fast with a capacity of 1000 packets (as in Linux).
	//string deviceQueueSize = "100p";			//this is the default value in ns3. Check the value in real world Cisco routers.
	uint32_t numberOfCoreRouters = 2;
	string topology = "dumbbell";
	double randomLossRate = 0.0;
	int trial = 4;
	uint32_t numberOfAgents = 4;

	CommandLine cmd;
	cmd.AddValue("transportProtocol", "Transport protocol to use: TcpNewReno, "
			"TcpHybla, TcpHighSpeed, TcpHtcp, TcpVegas, TcpScalable, TcpVeno, "
			"TcpBic, TcpYeah, TcpIllinois, TcpWestwood, TcpWestwoodPlus, TcpLedbat, "
			"TcpLp, TcpDctcp, TcpBbr, TcpCompound", tcps);
	cmd.AddValue("runtime", "How long the applications should send data", runtime);
	cmd.AddValue("longFlows", "Number of long-lived TCP flows", longFlows);
	cmd.AddValue("shortFlows","Number of short-lived TCP flows", shortFlows);
	cmd.AddValue("bottleneckLinkDataRate","The speed at which the bottleneck link serves the data (in Mbps)", bottleneckLinkDataRate);
	cmd.AddValue("bottleneckLinkDelay","The delay at the bottleneck link (in ms)", bottleneckLinkDelay);
	cmd.AddValue("endNodeLinkCapacity","The maximum data rate allowed at the source and destination links (in ms)", endNodeLinkCapacity);
	cmd.AddValue("endNodeLinkDelay","The delay at the source and destination links (in ms)", delays);
	cmd.AddValue("queueDiscType", "Bottleneck queue disc type in {PfifoFast, ARED, CoDel, FqCoDel, PIE, prio}", queueDiscType);
	cmd.AddValue("queueSize","Mention the queue size for the bottleneck link", queueSize);
	cmd.AddValue("mtu", "Size of IP packets to send in bytes", mtu);
	cmd.AddValue("numberOfPackets", "For Short flows specify the number of Packets to finish the connection", numberOfPackets);
	cmd.AddValue("tracing", "Pcap and Animation tracing", tracing);
	cmd.AddValue("traceSamplingTime", "Specify time period for observing the queue build up", traceSamplingTime);
	cmd.AddValue("isSack", "Selective Acknowledgment", isSack);
	cmd.AddValue("isDataRateVariable", "Put true to keep the router link's datarate variable", isDataRateVariable);
	cmd.AddValue("isRandomLossVariable", "Put true to keep the router link's lossrate variable" , isRandomLossVariable);
	cmd.AddValue("randomLossRate", "Random losses attribute in the core's links", randomLossRate);
	cmd.AddValue("isDelayVariable", "Put true to keep the router link's RTT variable" , isDelayVariable);
	cmd.AddValue("isRandomArrival", "Are the schedule times are random?", isRandomArrival);
	cmd.AddValue("numberOfCoreRouters", "The number of core nodes in backbone", numberOfCoreRouters);
	cmd.AddValue("topology", "Available topologies are:{singleLink, dumbbell, parkinglot}", topology);
	cmd.AddValue("trial", "Run number for multiple simulations", trial);
	cmd.AddValue("numberOfAgents", "Number for simulations", numberOfAgents);

	cmd.Parse (argc, argv);
	while (!delays.empty()) {
		if((delays.find(",")) != string::npos){
			endNodeLinkDelay.insert(Time(delays.substr(0, delays.find(","))));
			delays.erase(0, delays.find(",") + 1);
		}
		else{
			endNodeLinkDelay.insert(Time(delays));
			delays.clear();
		}
	}
	while (!tcps.empty()) {
		if((tcps.find(",")) != string::npos){
			transportProtocol.push_back(string ("ns3::") + string(tcps.substr(0, tcps.find(","))));
			NS_LOG_INFO("TCP variant is:" << TypeId::LookupByName (transportProtocol.back()));
			tcps.erase(0, tcps.find(",") + 1);
		}
		else{
			transportProtocol.push_back(string ("ns3::") + string(tcps));
			NS_LOG_INFO("TCP variant is:" << TypeId::LookupByName (transportProtocol.back()));
			tcps.clear();
		}
	}
	if(transportProtocol.size() > 1)
	{
		NS_ASSERT_MSG(longFlows == (uint32_t)transportProtocol.size(),
				"For multiple TCP variants to co-exist, number of long flows should be equal to number of variants declared");
	}

	RngSeedManager::SetSeed(4);
	RngSeedManager::SetRun(trial);

	//when Internet stacks are added to the node, the default TCP implementation that is aggregated to the node is the ns-3 TCP. This can be overridden as we show below when using Network Simulation Cradle.
	Config::SetDefault ("ns3::TcpL4Protocol::SocketType", TypeIdValue (TypeId::LookupByName (transportProtocol.back())));	// Last element will be universal
	Config::SetDefault ("ns3::TcpSocket::SegmentSize", UintegerValue (mtu));
	Config::SetDefault ("ns3::TcpSocketBase::Sack", BooleanValue (isSack));
	Config::SetDefault ("ns3::TcpSocketBase::WindowScaling", BooleanValue (true));						// Enabling window scaling option to increase the window size to 1Gbits in high bandwidth delay networks
	Config::SetDefault ("ns3::TcpSocketBase::LimitedTransmit", BooleanValue (false));
	Config::SetDefault ("ns3::TcpSocket::DelAckCount", UintegerValue (1));								// Number of packets to wait before sending an acknowledgment
	Config::SetDefault ("ns3::Ipv4L3Protocol::DefaultTtl", UintegerValue (64));							// Time to live = 64 means each router decreases this pkt by 1.
	Config::SetDefault ("ns3::TcpSocket::InitialCwnd", UintegerValue (1));								// Initial Cwnd = 10 pkts
	Config::SetDefault ("ns3::DropTailQueue<Packet>::MaxSize", QueueSizeValue (QueueSize ("1p")));		// Change the Hardware Buffer Capacity, if we choose to have different sizes for different links

	///////////////////////////////////////////////////////////////////////////
	//                                                                       //
	// 						Construct the backbone                           //
	//                                                                       //
	///////////////////////////////////////////////////////////////////////////
	/*
	 * TODO: Include wireless links for measuring its performance against wired in dumbbell.
	 */
	NS_LOG_INFO ("Creating links (cables, satellite) and nodes at the core");
	NodeContainer coreNodes;
	coreNodes.Create(numberOfCoreRouters);

	vector<NodeContainer> routerLinkContainer;															// clubbing two adjacent nodes together for a better representation of a link
	for(uint32_t eachCoreNode = 0; eachCoreNode < coreNodes.GetN()-1; eachCoreNode++){
		routerLinkContainer.push_back(NodeContainer(coreNodes.Get(eachCoreNode), coreNodes.Get(eachCoreNode+1)));
	}

	PointToPointHelper routerLink;																// check the network for Large bandwidth delay product >= 10^5 bits
	routerLink.SetDeviceAttribute("DataRate", DataRateValue(bottleneckLinkDataRate));
	routerLink.SetChannelAttribute("Delay", TimeValue(bottleneckLinkDelay));					// usually this bottleneck link delay is comparable to RTT like 100ms
	//routerLink.SetQueue("ns3::DropTailQueue", "MaxSize", StringValue(deviceQueueSize)); 		// hardware(physical) buffer, default is 100 packets

	NetDeviceContainer routerLinkDevices;
	for(vector<NodeContainer>::iterator eachLink = routerLinkContainer.begin(); eachLink != routerLinkContainer.end(); ++eachLink){
		routerLinkDevices.Add(routerLink.Install(*eachLink));				// This topology is linear with links connecting adjacent nodes
	}

	if(randomLossRate > 0){
		NS_LOG_INFO ("Introducing some ambiguity in reception of packets at bottleneck link");
		Ptr<RateErrorModel> errorIntroduced = CreateObject<RateErrorModel> ();
		errorIntroduced -> SetAttribute ("ErrorRate", DoubleValue (randomLossRate));

		for(NetDeviceContainer::Iterator eachDevice = routerLinkDevices.Begin(); eachDevice != routerLinkDevices.End(); ++eachDevice){
			(*eachDevice) -> SetAttribute ("ReceiveErrorModel", PointerValue (errorIntroduced));
		}
	}

	///////////////////////////////////////////////////////////////////////////
	//                                                                       //
	// 				Construct the Endpoint clusters                          //
	//                                                                       //
	///////////////////////////////////////////////////////////////////////////

	uint32_t numberOfTCPNodes = longFlows + shortFlows;
	vector<NodeContainer> dumbbellSenderNodes(routerLinkContainer.size()), dumbbellReceiverNodes(routerLinkContainer.size());
	NodeContainer manyToOneSenderNodes;
	Ptr<Node> manyToOneReceiverNode;

	if(topology.compare("dumbbell") == 0){
		NS_LOG_INFO("Building dumbbell topology with " << numberOfCoreRouters << " routers");
	}
	else if(topology.compare("singleLink") == 0){
		NS_LOG_INFO("Just a single link topology");
		NS_ASSERT(coreNodes.GetN() == 2);
		//NS_ASSERT_MSG(numberOfTCPNodes > 1, "A single one-to-one link can't accomodate multiple senders");
		endNodeLinkCapacity = bottleneckLinkDataRate;
		manyToOneSenderNodes.Add(coreNodes.Get(0));
		manyToOneReceiverNode = coreNodes.Get(1);
	}
	else if(topology.compare("parkinglot") == 0){
		NS_LOG_INFO("Building parking-lot topology");
		manyToOneSenderNodes.Create(numberOfTCPNodes);
		manyToOneReceiverNode = CreateObject<Node> ();
	}
	else{
		NS_FATAL_ERROR("Available topologies are:{singleLink, dumbbell, parkinglot}");
	}

	PointToPointHelper routerToEndpointLink;
	routerToEndpointLink.SetDeviceAttribute("DataRate", DataRateValue(endNodeLinkCapacity));
	routerToEndpointLink.SetChannelAttribute("Delay", TimeValue(*(endNodeLinkDelay.begin())));	// If there was only one value.

	NetDeviceContainer endNodeSenderDevices, endNodeReceiverDevices;

//-----------------------Dumbbell cluster configuration------------------------
	for(vector<NodeContainer>::iterator eachRouterLink = routerLinkContainer.begin(); eachRouterLink != routerLinkContainer.end(); ++eachRouterLink){
		if(topology.compare("dumbbell") == 0 || topology.compare("parkinglot") == 0){
			dumbbellSenderNodes[eachRouterLink - routerLinkContainer.begin()].Create(numberOfTCPNodes);
			dumbbellReceiverNodes[eachRouterLink - routerLinkContainer.begin()].Create(numberOfTCPNodes);
		}

		for(pair<NodeContainer::Iterator, set<Time>::iterator> eachSender(dumbbellSenderNodes[eachRouterLink - routerLinkContainer.begin()].Begin(), endNodeLinkDelay.begin());
				eachSender.first != dumbbellSenderNodes[eachRouterLink - routerLinkContainer.begin()].End(); ++(eachSender.first), ++(eachSender.second))
		{
			routerToEndpointLink.SetChannelAttribute("Delay", TimeValue(*(eachSender.second)));			// Setting this once again here to test RTT Unfairness
			NetDeviceContainer routerToSenderLinkDevices = routerToEndpointLink.Install((*eachRouterLink).Get(0), *(eachSender.first));
			routerLinkDevices.Add(routerToSenderLinkDevices.Get(0));
			endNodeSenderDevices.Add(routerToSenderLinkDevices.Get(1));	// Later on we will assign IP address and attach TCP socket to each of these senders.
		}

		for(pair<NodeContainer::Iterator, set<Time>::iterator> eachReceiver (dumbbellReceiverNodes[eachRouterLink - routerLinkContainer.begin()].Begin(), endNodeLinkDelay.begin());
				eachReceiver.first != dumbbellReceiverNodes[eachRouterLink - routerLinkContainer.begin()].End(); ++(eachReceiver.first), ++(eachReceiver.second))
		{
			routerToEndpointLink.SetChannelAttribute("Delay", TimeValue(*(eachReceiver.second)));
			NetDeviceContainer routerToReceiverLinkDevices = routerToEndpointLink.Install((*eachRouterLink).Get(1), *(eachReceiver.first));
			routerLinkDevices.Add(routerToReceiverLinkDevices.Get(0));
			endNodeReceiverDevices.Add(routerToReceiverLinkDevices.Get(1));
		}
	}
//-----------------------Dumbbell cluster configuration------------------------
//-----------------------Many-to-one cluster configuration------------------------
	if(manyToOneSenderNodes.GetN() > 0 && !(topology.compare("singleLink") == 0)){
		for(uint32_t eachManyToOneSender = 0; eachManyToOneSender < manyToOneSenderNodes.GetN(); ++eachManyToOneSender){
			NetDeviceContainer routerToSenderLinkDevices = routerToEndpointLink.Install(coreNodes.Get(0), manyToOneSenderNodes.Get(eachManyToOneSender));
			routerLinkDevices.Add(routerToSenderLinkDevices.Get(0));
			endNodeSenderDevices.Add(routerToSenderLinkDevices.Get(1));	// This adds to the first links' sender devices
		}

		NetDeviceContainer ndc = routerToEndpointLink.Install(coreNodes.Get(numberOfCoreRouters-1), manyToOneReceiverNode);
		routerLinkDevices.Add(ndc.Get(0));
		endNodeReceiverDevices.Add(ndc.Get(1)); // This adds to the last links' receiver devices
	}

//-----------------------Many-to-one cluster configuration------------------------
	vector<uint32_t> senderNodeIds;
	for(vector<NodeContainer>::iterator eachNodeContainer = dumbbellSenderNodes.begin(); eachNodeContainer != dumbbellSenderNodes.end(); eachNodeContainer++){
		for(NodeContainer::Iterator eachSender = eachNodeContainer->Begin(); eachSender != eachNodeContainer->End(); eachSender++){
			senderNodeIds.push_back((*eachSender)->GetId());
		}
	}
	for(NodeContainer::Iterator eachNode = manyToOneSenderNodes.Begin(); eachNode != manyToOneSenderNodes.End(); eachNode++){
		senderNodeIds.push_back((*eachNode)->GetId());
	}

	if(numberOfAgents>0)
	{
		NS_LOG_INFO ("Introducing RL agents into our simulator");
		string assignAgentsToNodes;
		Ptr<UniformRandomVariable> randomVariable = CreateObject<UniformRandomVariable> ();

		for(vector<uint32_t>::iterator eachSenderId = senderNodeIds.begin(); eachSenderId != senderNodeIds.end(); eachSenderId++){
			string str = "Agent"+to_string(randomVariable->GetInteger(0, numberOfAgents));
			assignAgentsToNodes.append(to_string(*eachSenderId)+":"+str+",");
		}

		Config::SetDefault ("ns3::TcpMadras::AgentMap", StringValue(assignAgentsToNodes));
	}

	NS_LOG_INFO ("Installing Internet Protocol stacks on all nodes");
	InternetStackHelper internetStackHelper;
	internetStackHelper.InstallAll();

	///////////////////////////////////////////////////////////////////////////
	//                                                                       //
	// 				Configuring the Network stack                            //
	//                                                                       //
	///////////////////////////////////////////////////////////////////////////
//-----------------------Installing traffic control------------------------
	NS_LOG_INFO("Assigning an AQM for all the netdevices in the core nodes");
	TrafficControlHelper bottleneckLinkAQM/*, accessLinkAQM*/;

	if (queueDiscType.compare ("PfifoFast") == 0)
	{
		bottleneckLinkAQM.SetRootQueueDisc ("ns3::PfifoFastQueueDisc");				// software(NIC device's) queue, apply this before assigning the IPV4 addresses
		Config::SetDefault ("ns3::PfifoFastQueueDisc::MaxSize",
				QueueSizeValue (QueueSize (queueSize)));
	}
	else if (queueDiscType.compare ("ARED") == 0)
	{
		bottleneckLinkAQM.SetRootQueueDisc ("ns3::RedQueueDisc");
		Config::SetDefault ("ns3::RedQueueDisc::ARED", BooleanValue (true));
		Config::SetDefault ("ns3::RedQueueDisc::MaxSize",
				QueueSizeValue (QueueSize (queueSize)));
		Config::SetDefault ("ns3::RedQueueDisc::UseEcn", BooleanValue (true));
	}
	else if (queueDiscType.compare ("CoDel") == 0)
	{
		bottleneckLinkAQM.SetRootQueueDisc ("ns3::CoDelQueueDisc");
		Config::SetDefault ("ns3::CoDelQueueDisc::MaxSize",
				QueueSizeValue (QueueSize (queueSize)));
	}
	else if (queueDiscType.compare ("FqCoDel") == 0)
	{
		bottleneckLinkAQM.SetRootQueueDisc ("ns3::FqCoDelQueueDisc");
		Config::SetDefault ("ns3::FqCoDelQueueDisc::MaxSize",
				QueueSizeValue (QueueSize (queueSize)));
	}
	else if (queueDiscType.compare ("PIE") == 0)
	{
		bottleneckLinkAQM.SetRootQueueDisc ("ns3::PieQueueDisc");
		Config::SetDefault ("ns3::PieQueueDisc::MaxSize",
				QueueSizeValue (QueueSize (queueSize)));
	}
	else if (queueDiscType.compare ("prio") == 0)
	{
		uint16_t handle = bottleneckLinkAQM.SetRootQueueDisc ("ns3::PrioQueueDisc", "Priomap",
				StringValue ("0 1 0 1 0 1 0 1 0 1 0 1 0 1 0 1"));
		TrafficControlHelper::ClassIdList cid = bottleneckLinkAQM.AddQueueDiscClasses (handle, 2, "ns3::QueueDiscClass");
		bottleneckLinkAQM.AddChildQueueDisc (handle, cid[0], "ns3::FifoQueueDisc");
		bottleneckLinkAQM.AddChildQueueDisc (handle, cid[1], "ns3::RedQueueDisc");
	}
	else
	{
		bottleneckLinkAQM.SetRootQueueDisc ("ns3::FifoQueueDisc");
		Config::SetDefault ("ns3::FifoQueueDisc::MaxSize",
				QueueSizeValue (QueueSize (queueSize)));
	}

	QueueDiscContainer qDiscContainer = bottleneckLinkAQM.Install(routerLinkDevices);			// Installing these queue policies on all queues corresponding to the core nodes

//-----------------------Installing traffic control------------------------
//-----------------------Installing routing protocol------------------------
	NS_LOG_INFO ("Assign IP Addresses");
	Ipv4AddressHelper sourceIPAddresses, sinkIPAddresses, routerIPAddresses;
	sourceIPAddresses.SetBase ("192.168.10.0", "255.255.255.0");
	sinkIPAddresses.SetBase("172.1.1.0", "255.255.255.0");
	routerIPAddresses.SetBase("10.10.1.0","255.255.255.0");

	Ipv4InterfaceContainer routerIPs = routerIPAddresses.Assign(routerLinkDevices);
	Ipv4InterfaceContainer sourceIPs, receiverIPs;

	for(NetDeviceContainer::Iterator eachSenderDevice = endNodeSenderDevices.Begin(); eachSenderDevice != endNodeSenderDevices.End(); eachSenderDevice++){
		sourceIPs = sourceIPAddresses.Assign(*eachSenderDevice);
		sourceIPAddresses.NewNetwork();
	}

	for(NetDeviceContainer::Iterator eachReceiverDevice = endNodeReceiverDevices.Begin(); eachReceiverDevice != endNodeReceiverDevices.End(); ++eachReceiverDevice){
		receiverIPs = sinkIPAddresses.Assign(*eachReceiverDevice);
		sinkIPAddresses.NewNetwork();
	}
//-----------------------Installing routing protocol------------------------
	NS_LOG_INFO ("Enable static global routing");
	Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
	///////////////////////////////////////////////////////////////////////////
	//                                                                       //
	// 					Generating the Traffic		                         //
	//                                                                       //
	///////////////////////////////////////////////////////////////////////////

	NS_LOG_INFO ("Creating an Application");
	uint16_t receivingPortNumber = 8080;

	vector<vector<Ptr<Socket>>> sourceSocket(routerLinkContainer.size(), vector<Ptr<Socket>> (numberOfTCPNodes));
	ApplicationContainer destinationApp;

	double shape = 1.4;
	Ptr<ParetoRandomVariable> fileDistribution = CreateObject<ParetoRandomVariable> ();
	fileDistribution -> SetAttribute ("Scale", DoubleValue (numberOfPackets*(shape-1)/shape));		// E(X) = Shape*Scale / (Shape -1)
	fileDistribution -> SetAttribute ("Shape", DoubleValue (shape));								// Mean = 30 pkts, Shape = 1.4. Hence, Scale = 8.5714

	shape = 1.0;
	Ptr<WeibullRandomVariable> flowArrivalProcess = CreateObject<WeibullRandomVariable> ();
	flowArrivalProcess -> SetAttribute ("Scale", DoubleValue (runtime/10));		// E[value]  =  scale * Gamma(1 + 1 / shape); here mean is half of runtime
	flowArrivalProcess -> SetAttribute ("Shape", DoubleValue (shape));

	/* TODO:1) Include UDP flows
	 */
	double timeSlot = runtime/((2*longFlows)-1);
	for(vector<NodeContainer>::iterator eachRouterLink = routerLinkContainer.begin(); eachRouterLink != routerLinkContainer.end(); ++eachRouterLink)
	{
		for(uint32_t eachPair = 0; eachPair < dumbbellSenderNodes[eachRouterLink - routerLinkContainer.begin()].GetN(); ++eachPair)
		{
			Ipv4Address address = GetAddressFromNode(dumbbellReceiverNodes[eachRouterLink - routerLinkContainer.begin()].Get(eachPair));

			Address sinkLocalAddress(InetSocketAddress (address, receivingPortNumber));
			sourceSocket[eachRouterLink - routerLinkContainer.begin()][eachPair] = Socket::CreateSocket(dumbbellSenderNodes[eachRouterLink - routerLinkContainer.begin()].Get(eachPair), TcpSocketFactory::GetTypeId());
			sourceSocket[eachRouterLink - routerLinkContainer.begin()][eachPair] -> TraceConnectWithoutContext ("CongestionWindow", MakeBoundCallback (&CwndChange, dumbbellSenderNodes[eachRouterLink - routerLinkContainer.begin()].Get(eachPair) ->GetId()));
			sourceSocket[eachRouterLink - routerLinkContainer.begin()][eachPair] -> TraceConnectWithoutContext("RTT", MakeBoundCallback (&PingRtt, dumbbellSenderNodes[eachRouterLink - routerLinkContainer.begin()].Get(eachPair) ->GetId()));
			sourceSocket[eachRouterLink - routerLinkContainer.begin()][eachPair] -> TraceConnectWithoutContext("Tx", MakeBoundCallback (&TraceTx, dumbbellSenderNodes[eachRouterLink - routerLinkContainer.begin()].Get(eachPair) ->GetId()));
			sourceSocket[eachRouterLink - routerLinkContainer.begin()][eachPair] -> TraceConnectWithoutContext("Rx", MakeBoundCallback (&TraceRx, dumbbellSenderNodes[eachRouterLink - routerLinkContainer.begin()].Get(eachPair) ->GetId()));

			Ptr<MyApp> myAppObject = CreateObject<MyApp> ();
			if (eachPair<longFlows)
			{
				if(transportProtocol.size() > 1)	// The last element is already the default TCP, so need not include for the last flow.
				{
					ObjectFactory congestionAlgorithmFactory;
					congestionAlgorithmFactory.SetTypeId(TypeId::LookupByName ((string)transportProtocol.front()));
					transportProtocol.pop_front();
					Ptr<TcpCongestionOps> algorithm = congestionAlgorithmFactory.Create<TcpCongestionOps>();

					Ptr<TcpSocket> tcpSocket = DynamicCast<TcpSocket>(sourceSocket[eachRouterLink - routerLinkContainer.begin()][eachPair]);
					Ptr<TcpSocketBase> tcpSocketBase = DynamicCast<TcpSocketBase>(tcpSocket);
					tcpSocketBase -> SetCongestionControlAlgorithm(algorithm);
				}
				myAppObject -> Setup(sourceSocket[eachRouterLink - routerLinkContainer.begin()][eachPair], sinkLocalAddress, mtu, 0, DataRate (10*endNodeLinkCapacity.GetBitRate()));		// Here application prepares 10 times the access link's bandwidth to the TCP socket's buffer.
				dumbbellSenderNodes[eachRouterLink - routerLinkContainer.begin()].Get(eachPair) -> AddApplication(myAppObject);
				if(isRandomArrival){
					myAppObject -> SetStartTime(Seconds (flowArrivalProcess -> GetValue ()));
					myAppObject -> SetStopTime(Seconds (runtime));
				}
				else{
					myAppObject -> SetStartTime(Seconds (eachPair*timeSlot));
					myAppObject -> SetStopTime(Seconds (runtime - eachPair*timeSlot));
				}
			}
			else{
				myAppObject -> Setup(sourceSocket[eachRouterLink - routerLinkContainer.begin()][eachPair], sinkLocalAddress, mtu, fileDistribution -> GetValue(), DataRate (10*endNodeLinkCapacity.GetBitRate()));
				dumbbellSenderNodes[eachRouterLink - routerLinkContainer.begin()].Get(eachPair) -> AddApplication(myAppObject);
				myAppObject -> SetStartTime(Seconds (flowArrivalProcess -> GetValue ()));
				myAppObject -> SetStopTime(Seconds (runtime));
			}

			PacketSinkHelper sinkHelper ("ns3::TcpSocketFactory", sinkLocalAddress);
			sinkHelper.SetAttribute ("Protocol", TypeIdValue (TcpSocketFactory::GetTypeId ()));
			destinationApp.Add(sinkHelper.Install(dumbbellReceiverNodes[eachRouterLink - routerLinkContainer.begin()].Get(eachPair)));		/*dumbbell.GetRight(eachSender))*/
			destinationApp.Start(Seconds(0.01));
			destinationApp.Stop(Seconds(runtime));
		}
	}

	for(NodeContainer::Iterator manyToOneSender = manyToOneSenderNodes.Begin(); manyToOneSender != manyToOneSenderNodes.End(); manyToOneSender++)
	{
		Ipv4Address receiverAddress = GetAddressFromNode(manyToOneReceiverNode);

		Address sinkLocalAddress(InetSocketAddress (receiverAddress, receivingPortNumber++));	// port number to be changed
		sourceSocket[0][manyToOneSender - manyToOneSenderNodes.Begin()] = Socket::CreateSocket((*manyToOneSender), TcpSocketFactory::GetTypeId());
		sourceSocket[0][manyToOneSender - manyToOneSenderNodes.Begin()] -> TraceConnectWithoutContext ("CongestionWindow", MakeBoundCallback (&CwndChange, (*manyToOneSender) ->GetId()));
		sourceSocket[0][manyToOneSender - manyToOneSenderNodes.Begin()] -> TraceConnectWithoutContext ("RTT", MakeBoundCallback (&PingRtt, (*manyToOneSender) ->GetId()));
		sourceSocket[0][manyToOneSender - manyToOneSenderNodes.Begin()] -> TraceConnectWithoutContext ("Tx", MakeBoundCallback (&TraceTx, (*manyToOneSender) ->GetId()));
		sourceSocket[0][manyToOneSender - manyToOneSenderNodes.Begin()] -> TraceConnectWithoutContext ("Rx", MakeBoundCallback (&TraceRx, (*manyToOneSender) ->GetId()));

		Ptr<MyApp> myAppObject = CreateObject<MyApp> ();
		myAppObject -> Setup(sourceSocket[0][manyToOneSender - manyToOneSenderNodes.Begin()], sinkLocalAddress, mtu, 0, DataRate (10*endNodeLinkCapacity.GetBitRate()));
		(*manyToOneSender) -> AddApplication(myAppObject);
		myAppObject -> SetStartTime(Seconds (flowArrivalProcess -> GetValue ()));
		myAppObject -> SetStopTime(Seconds (runtime));

		PacketSinkHelper sinkHelper ("ns3::TcpSocketFactory", sinkLocalAddress);
		sinkHelper.SetAttribute ("Protocol", TypeIdValue (TcpSocketFactory::GetTypeId ()));
		destinationApp.Add(sinkHelper.Install(manyToOneReceiverNode));	// Every sender is sending to last router's single node
		destinationApp.Start(Seconds(0.01));
		destinationApp.Stop(Seconds(runtime));
	}

	///////////////////////////////////////////////////////////////////////////
	//                                                                       //
	// 						Data Analytics				                     //
	//                                                                       //
	///////////////////////////////////////////////////////////////////////////

	if(tracing)										// For WIRESHARK and to plot I/O statistics graphs
	{
		NS_LOG_INFO ("Wireshark Trace file (.pcap) is generated");
		routerToEndpointLink.EnablePcapAll(fileName+"SourcePcap", false);
		//routerToSinkLink.EnablePcapAll(fileName+"SinkPcap", false);
	}

	AsciiTraceHelper asciiTraceHelper;
//------------------User Observables---------------------------------------------
	streamWindow = asciiTraceHelper.CreateFileStream(fileName+"Window.csv");
	//streamOneWayDelay = asciiTraceHelper.CreateFileStream(fileName+"Delay.csv");
	streamFlowMonitor = asciiTraceHelper.CreateFileStream(fileName+"FlowMonitor.dat");
	streamTxData = asciiTraceHelper.CreateFileStream(fileName+"TxData.csv");
	streamAvgThroughput = asciiTraceHelper.CreateFileStream(fileName+"AvgThroughput.csv");
	streamThroughput = asciiTraceHelper.CreateFileStream(fileName+"Throughput.csv");
	streamRTT = asciiTraceHelper.CreateFileStream(fileName+"RTT.csv");
	streamFairness = asciiTraceHelper.CreateFileStream(fileName+"FairnessIndex.csv");
//------------------User Observables---------------------------------------------
//------------------Core Observables---------------------------------------------
	streamSoftwareQueue = asciiTraceHelper.CreateFileStream(fileName+"SoftwareQueue.csv");
	streamLinkDrops = asciiTraceHelper.CreateFileStream(fileName+"Drops.csv");
	streamLinkUtilization = asciiTraceHelper.CreateFileStream(fileName+"Utilization.csv");
	streamLinkCapacity = asciiTraceHelper.CreateFileStream(fileName+"LinkCapacity.csv");
//------------------Core Observables---------------------------------------------


	//Ptr<QueueDisc> queueDisc = routerLinkDevices.Get(0)->GetObject(TypeId("ns3::QueueDiscClass"));				/*leftRouterQueueDiscs[0].Get(0)*/
	Ptr<QueueDisc> queueDisc = qDiscContainer.Get(0);
	queueDisc -> TraceConnectWithoutContext ("PacketsInQueue", MakeBoundCallback(&TraceAQMQueue, 0));
	queueDisc -> TraceConnectWithoutContext("Drop", MakeBoundCallback(&TraceRouterBufferLosses, 0));

	Ptr<NetDevice> leftRouterNetDevice = routerLinkDevices.Get(0);													/*dumbbell.GetLeft() ->GetDevice(0)*/
	Ptr<PointToPointNetDevice> leftP2pNetDevice = StaticCast<PointToPointNetDevice> (leftRouterNetDevice);
	Ptr<Queue<Packet>> hardwareQueue = leftP2pNetDevice -> GetQueue ();
	//hardwareQueue -> TraceConnectWithoutContext ("PacketsInQueue", MakeBoundCallback(&TraceNetDeviceQueue, 0));
	hardwareQueue -> TraceConnectWithoutContext("Dequeue", MakeCallback (&TracePacketsInLink));

	if(isDataRateVariable)
	{
		Time slidingWindowTime = Seconds(2.0);
		SlidingWindow<double> movingAverage(static_cast<int>(slidingWindowTime.GetDouble()/traceSamplingTime));
		//changing capacity every 0.5 second
		Simulator::Schedule (Seconds (0.5), &VariableDatarate, leftP2pNetDevice, bottleneckLinkDataRate, movingAverage, 0.5);
	}

	if(isDelayVariable)
	{
		Simulator::Schedule (Seconds (0.5), &VariableDelay, leftP2pNetDevice, traceSamplingTime);
	}

	if(isRandomLossVariable)
	{
		Simulator::Schedule (Seconds (0.5), &VariableRandomLoss, leftP2pNetDevice, traceSamplingTime);
	}

	NS_LOG_INFO("Monitoring each flow");
	FlowMonitorHelper flowmon;
	Ptr<FlowMonitor> monitor = flowmon.InstallAll();
	Ptr<FlowClassifier> flowClassifier = flowmon.GetClassifier();
	Ptr<Ipv4FlowClassifier> ipv4FlowClassifier = DynamicCast<Ipv4FlowClassifier>(flowClassifier); // upcasting a derived class to base class

	Simulator::Schedule(Seconds (0.0), &Configure, senderNodeIds, bottleneckLinkDataRate);
	Simulator::Schedule(Seconds (0.1), &RecordNetworkData, traceSamplingTime);
	Simulator::Schedule(Seconds (0.1), &RecordUserData, traceSamplingTime);

	Simulator::Stop (Seconds (runtime));
	NS_LOG_INFO ("Running Simulation");
	Simulator::Run ();

	monitor -> SerializeToXmlFile(fileName+"FlowMonitor.xml", false, false);

	map<FlowId, FlowMonitor::FlowStats> stats = monitor -> GetFlowStats ();				// a flow is defined as the packets with the same {protocol, source (IP, port), destination (IP, port)} tuple.
	*streamFlowMonitor -> GetStream() << endl << "*** Flow monitor statistics ***" << endl;
	for(map<uint32_t, FlowMonitor::FlowStats>::iterator flowId = stats.begin (); flowId != stats.end (); flowId++)
	{
		for(NodeContainer::Iterator eachNode = manyToOneSenderNodes.Begin(); eachNode != manyToOneSenderNodes.End(); ++eachNode)
		{
			if((ipv4FlowClassifier ->FindFlow(flowId->first)).sourceAddress == GetAddressFromNode(*eachNode))
			{
				*streamFlowMonitor -> GetStream() << "------------ \t" << flowId->first << "\t ----------------" << endl;
				*streamFlowMonitor -> GetStream() << "  Tx Packets/Bytes:   " << stats[flowId->first].txPackets << " / " << stats[flowId->first].txBytes << endl;
				*streamFlowMonitor -> GetStream() << "  Throughput: " << stats[flowId->first].txBytes * 8.0 / (stats[flowId->first].timeLastTxPacket.GetSeconds () - stats[flowId->first].timeFirstTxPacket.GetSeconds ()) / pow(2,20) << " Mbps" << endl;
				*streamFlowMonitor -> GetStream() << "  Rx Packets/Bytes:   " << stats[flowId->first].rxPackets << " / " << stats[flowId->first].rxBytes << endl;

				uint32_t packetsDroppedByQueueDisc = 0;
				uint64_t bytesDroppedByQueueDisc = 0;
				if (stats[flowId->first].packetsDropped.size () > Ipv4FlowProbe::DROP_QUEUE_DISC)
				{
					packetsDroppedByQueueDisc = stats[flowId->first].packetsDropped[Ipv4FlowProbe::DROP_QUEUE_DISC];
					bytesDroppedByQueueDisc = stats[flowId->first].bytesDropped[Ipv4FlowProbe::DROP_QUEUE_DISC];
				}
				*streamFlowMonitor -> GetStream() << "  Packets/Bytes Dropped by Queue Disc:   " << packetsDroppedByQueueDisc << " / " << bytesDroppedByQueueDisc << endl;

				uint32_t packetsDroppedByNetDevice = 0;
				uint64_t bytesDroppedByNetDevice = 0;
				if (stats[flowId->first].packetsDropped.size () > Ipv4FlowProbe::DROP_QUEUE)
				{
					packetsDroppedByNetDevice = stats[flowId->first].packetsDropped[Ipv4FlowProbe::DROP_QUEUE];
					bytesDroppedByNetDevice = stats[flowId->first].bytesDropped[Ipv4FlowProbe::DROP_QUEUE];
				}
				*streamFlowMonitor -> GetStream() << "  Packets/Bytes Dropped by NetDevice:   " << packetsDroppedByNetDevice << " / " << bytesDroppedByNetDevice << endl;

				*streamFlowMonitor -> GetStream() << "  Goodput: " << stats[flowId->first].rxBytes * 8.0 / (stats[flowId->first].timeLastRxPacket.GetSeconds () - stats[flowId->first].timeFirstRxPacket.GetSeconds ()) / pow(2,20) << " Mbps" << endl;
				*streamFlowMonitor -> GetStream() << "  Mean delay:   " << stats[flowId->first].delaySum.GetSeconds () * 1000.0 / stats[flowId->first].rxPackets << " msec" << endl;
				*streamFlowMonitor -> GetStream() << "  Mean jitter:   " << stats[flowId->first].jitterSum.GetSeconds () * 1000.0 / (stats[flowId->first].rxPackets - 1) << " msec" << endl;
				*streamFlowMonitor -> GetStream() << "  Flow completion time:   " << stats[flowId->first].timeLastTxPacket.GetSeconds() - stats[flowId->first].timeFirstTxPacket.GetSeconds() << " sec" << endl;
				*streamFlowMonitor -> GetStream() << "\n" << endl;
				break;
			}
		}
		for(vector<NodeContainer>::iterator eachRouterLink = routerLinkContainer.begin(); eachRouterLink != routerLinkContainer.end(); ++eachRouterLink)
			{
				for(uint32_t eachSender=0; eachSender < dumbbellSenderNodes[eachRouterLink - routerLinkContainer.begin()].GetN(); ++eachSender)
				{
					if((ipv4FlowClassifier ->FindFlow(flowId->first)).sourceAddress == GetAddressFromNode(dumbbellSenderNodes[eachRouterLink - routerLinkContainer.begin()].Get(eachSender)))
					{
						*streamFlowMonitor -> GetStream() << "------------ \t" << flowId->first << "\t ----------------" << endl;
						*streamFlowMonitor -> GetStream() << "  Tx Packets/Bytes:   " << stats[flowId->first].txPackets << " / " << stats[flowId->first].txBytes << endl;
						*streamFlowMonitor -> GetStream() << "  Throughput: " << stats[flowId->first].txBytes * 8.0 / (stats[flowId->first].timeLastTxPacket.GetSeconds () - stats[flowId->first].timeFirstTxPacket.GetSeconds ()) / pow(2,20) << " Mbps" << endl;
						*streamFlowMonitor -> GetStream() << "  Rx Packets/Bytes:   " << stats[flowId->first].rxPackets << " / " << stats[flowId->first].rxBytes << endl;

						uint32_t packetsDroppedByQueueDisc = 0;
						uint64_t bytesDroppedByQueueDisc = 0;
						if (stats[flowId->first].packetsDropped.size () > Ipv4FlowProbe::DROP_QUEUE_DISC)
						{
							packetsDroppedByQueueDisc = stats[flowId->first].packetsDropped[Ipv4FlowProbe::DROP_QUEUE_DISC];
							bytesDroppedByQueueDisc = stats[flowId->first].bytesDropped[Ipv4FlowProbe::DROP_QUEUE_DISC];
						}
						*streamFlowMonitor -> GetStream() << "  Packets/Bytes Dropped by Queue Disc:   " << packetsDroppedByQueueDisc << " / " << bytesDroppedByQueueDisc << endl;

						uint32_t packetsDroppedByNetDevice = 0;
						uint64_t bytesDroppedByNetDevice = 0;
						if (stats[flowId->first].packetsDropped.size () > Ipv4FlowProbe::DROP_QUEUE)
						{
							packetsDroppedByNetDevice = stats[flowId->first].packetsDropped[Ipv4FlowProbe::DROP_QUEUE];
							bytesDroppedByNetDevice = stats[flowId->first].bytesDropped[Ipv4FlowProbe::DROP_QUEUE];
						}
						*streamFlowMonitor -> GetStream() << "  Packets/Bytes Dropped by NetDevice:   " << packetsDroppedByNetDevice << " / " << bytesDroppedByNetDevice << endl;

						*streamFlowMonitor -> GetStream() << "  Goodput: " << stats[flowId->first].rxBytes * 8.0 / (stats[flowId->first].timeLastRxPacket.GetSeconds () - stats[flowId->first].timeFirstRxPacket.GetSeconds ()) / pow(2,20) << " Mbps" << endl;
						*streamFlowMonitor -> GetStream() << "  Mean delay:   " << stats[flowId->first].delaySum.GetSeconds () * 1000.0 / stats[flowId->first].rxPackets << " msec" << endl;
						*streamFlowMonitor -> GetStream() << "  Mean jitter:   " << stats[flowId->first].jitterSum.GetSeconds () * 1000.0 / (stats[flowId->first].rxPackets - 1) << " msec" << endl;
						*streamFlowMonitor -> GetStream() << "  Flow completion time:   " << stats[flowId->first].timeLastTxPacket.GetSeconds() - stats[flowId->first].timeFirstTxPacket.GetSeconds() << " sec" << endl;
						*streamFlowMonitor -> GetStream() << "\n" << endl;
						break;
					}
				}
			}
	}
	Simulator::Destroy ();

	NS_LOG_INFO("Done");
	return 0;
}
